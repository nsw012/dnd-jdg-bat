package jp.co.d2c.dnd.springconfig;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import jp.co.d2c.dnd.util.S3Accessor;
import jp.co.d2c.dnd.util.S3GrantedAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;


/**
 * AWS関連の設定を管理するクラス
 */
@Configuration
public class AwsConfig {

  @Autowired
  private Environment environment;

  @Bean
  public AWSCredentialsProvider awsCredentialsProvider() {
    if (environment.acceptsProfiles("local")) {
      return new AWSCredentialsProviderChain(new EnvironmentVariableCredentialsProvider());
    } else {
      return new AWSCredentialsProviderChain(new InstanceProfileCredentialsProvider());
    }
  }

  @Bean
  @Autowired
  public S3Accessor s3Accessor(Environment environment, AWSCredentialsProvider awsCredentialsProvider) {
    AmazonS3 amazonS3 = new AmazonS3Client(awsCredentialsProvider);
    amazonS3.setRegion(RegionUtils.getRegion(environment.getProperty("aws.region")));
    return new S3GrantedAccessor(amazonS3
      , environment.getProperty("aws.s3.retryCount", Integer.class, 3)
      , environment.getProperty("aws.s3.retryInterval.millSec", Integer.class, 1000)
      , environment.getProperty("aws.s3.preSignedUrl.expireTime.millSec", Integer.class, 1000));
  }

  @Bean
  public ObjectMapper objectMapper() {
    return Jackson2ObjectMapperBuilder
      .json()
      .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
      .featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_INDEX)
      .build();
  }
}