/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.springconfig;

import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.util.DateTimeSupplier;
import jp.co.d2c.dnd.util.MessageHelper;
import jp.co.d2c.dnd.util.XmlResourceControl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.support.ResourcePropertySource;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;

/**
 * アプリケーションを構成する基本クラス
 */
@Configuration
@ComponentScan(basePackages = {"jp.co.d2c.dnd.batch"}, lazyInit = true)
public class AppConfig {

  protected String environmentLocation() {
    return "classpath:/config/environment.properties";
  }

  protected String environmentApiLocation() {
    return null;
  }

  @Bean
  @Autowired
  public Environment environment() {
    StandardEnvironment environment = new StandardEnvironment();

    try {
      environment.getPropertySources().addLast(new ResourcePropertySource(environmentLocation()));

      if (environmentApiLocation() != null) {
        environment.getPropertySources().addLast(new ResourcePropertySource(environmentApiLocation()));
      }
    } catch (IOException e) {
      throw new DndRuntimeException(e);
    }

    return environment;
  }
  @Bean
  public PropertiesPropertySource applicationServerSettings() {
    try {
      return new ResourcePropertySource(environmentLocation());
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  @Bean
  @Autowired
  public DateTimeSupplier dateTimeSupplier(Environment environment) {
    return new DateTimeSupplier();
  }

  @PostConstruct
  public void init() {
    setupMessageHelper();
  }

  private void setupMessageHelper() {
    MessageHelper.setResourceBundle(ResourceBundle.getBundle("messages/log-messages", new XmlResourceControl()));
  }
}
