package jp.co.d2c.dnd.springconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * サービスを構成するクラス
 */
@Configuration
@ComponentScan(basePackages = {"jp.co.d2c.dnd.service", "jp.co.d2c.dnd.dao"}, lazyInit = true)
public class ServiceConfig {

}
