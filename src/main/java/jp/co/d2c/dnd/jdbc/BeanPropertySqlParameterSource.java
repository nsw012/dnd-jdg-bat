/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.jdbc;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;

import jp.co.d2c.dnd.constant.CodeConstant;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * ビーンプロパティからパラメータ値を取得するSQLパラメータソースの実装クラス
 */
public class BeanPropertySqlParameterSource extends org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource {

  public BeanPropertySqlParameterSource(Object object) {
    super(object);
  }

  @Override
  public Object getValue(String paramName) {
    Object value = null;

    try {
      value = super.getValue(camelize(paramName));
    } catch (IllegalArgumentException e) {
      // ignore
    }

    if (value instanceof LocalDateTime) {
      return java.sql.Timestamp.valueOf((LocalDateTime) value);
    } else if (value instanceof LocalDate) {
      return java.sql.Date.valueOf((LocalDate) value);
    } else if (value instanceof LocalTime) {
      return java.sql.Time.valueOf((LocalTime) value);
    } else if (value instanceof CodeConstant) {
      return ((CodeConstant<?>) value).getValue();
    }

    return value;
  }

  @Override
  public int getSqlType(String paramName) {
    return super.getSqlType(camelize(paramName));
  }

  private static String camelize(String snake) {
    return LOWER_UNDERSCORE.to(LOWER_CAMEL, snake);
  }
}
