/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.jdbc;

import jp.co.d2c.dnd.constant.CodeConstant;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import java.beans.PropertyDescriptor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * リザルトセットの行をエンティティにバインドするためのマッパクラス
 *
 * @param <T> マップ対象のクラス
 */
public class DndPropertyRowMapper<T> extends org.springframework.jdbc.core.BeanPropertyRowMapper<T> {

  public DndPropertyRowMapper() {
    super();
  }

  public DndPropertyRowMapper(Class<T> mappedClass) {
    super(mappedClass);
  }

  @Override
  protected Object getColumnValue(ResultSet resultSet, int index, PropertyDescriptor descriptor) throws SQLException {
    if (resultSet == null) {
      return null;
    }

    Class<?> current = descriptor.getPropertyType();

    if (CodeConstant.class.isAssignableFrom(current)) {
      try {
        @SuppressWarnings("rawtypes")
        CodeConstant codeConstant = (CodeConstant) current.newInstance();
        return codeConstant.valueOf(resultSet.getInt(index));
      } catch (InstantiationException | IllegalAccessException e) {
        e.printStackTrace();
      }
    } else if (current.equals(LocalDateTime.class)) {
      Timestamp value = resultSet.getTimestamp(index);
      return value == null ? null : value.toLocalDateTime();
    }

    return super.getColumnValue(resultSet, index, descriptor);
  }
}
