/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.jdbc;

import jp.co.d2c.dnd.constant.CodeConstant;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * マップからパラメータ値を取得するSQLパラメータソースの実装クラス
 */
public class DndMapSqlParameterSource extends org.springframework.jdbc.core.namedparam.MapSqlParameterSource {

  @Override
  public Object getValue(String paramName) {
    Object value = null;

    try {
      value = super.getValue(paramName);
    } catch (IllegalArgumentException e) {
      // ignore
    }

    if (value instanceof LocalDateTime) {
      return java.sql.Timestamp.valueOf((LocalDateTime) value);
    } else if (value instanceof LocalDate) {
      return java.sql.Date.valueOf((LocalDate) value);
    } else if (value instanceof LocalTime) {
      return java.sql.Time.valueOf((LocalTime) value);
    } else if (value instanceof CodeConstant) {
      return ((CodeConstant<?>) value).getValue();
    }

    return value;
  }
}
