package jp.co.d2c.dnd.entity.tsv;

import lombok.Data;
import lombok.experimental.Accessors;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class ImportTsvBaseNative extends ImportTsvBase {

  private String title;
  private String img;
  private String sponsor;
  private String optout;
  private String link;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getSponsor() {
    return sponsor;
  }

  public void setSponsor(String sponsor) {
    this.sponsor = sponsor;
  }

  public String getOptout() {
    return optout;
  }

  public void setOptout(String optout) {
    this.optout = optout;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  @Override
  public String[] getPositionMapping() {
    return new String[]{"crid", "adm", "adomain", "cat", "attr", "title", "img", "sponsor", "optout", "link"};
  }
}
