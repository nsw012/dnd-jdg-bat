/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class MImp implements Serializable {

  private Long id;
  private Long adPageId;
  private BigDecimal floorPrice;
  private boolean secure;
  private Long bannerId;
  private Long nativeId;
  private Long videoId;
  private String tag;

}
