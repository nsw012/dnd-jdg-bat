/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class MAdPageBlockDomain implements Serializable {

  private Long id;
  private Long adPageId;
  private String domain;

}
