package jp.co.d2c.dnd.entity.tsv;

import lombok.Data;
import lombok.experimental.Accessors;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class ImportTsvBaseVideo extends ImportTsvBase {

  private String protocol;
  private String w;
  private String h;
  private String link;

  public String getProtocol() {
    return protocol;
  }

  public void setProtocol(String protocol) {
    this.protocol = protocol;
  }

  public String getW() {
    return w;
  }

  public void setW(String w) {
    this.w = w;
  }

  public String getH() {
    return h;
  }

  public void setH(String h) {
    this.h = h;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  @Override
  public String[] getPositionMapping() {
    return new String[]{"crid", "adm", "adomain", "cat", "attr", "protocol", "w", "h", "link"};
  }
}
