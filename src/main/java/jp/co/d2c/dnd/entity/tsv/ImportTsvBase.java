package jp.co.d2c.dnd.entity.tsv;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotBlank;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class ImportTsvBase {

  @NotBlank
  private String crid;
  @NotBlank
  private String adm;
  @NotBlank
  private String adomain;
  @NotBlank
  private String cat;
  private String attr;

  // TODO opencsvとlombokの相性が悪く、getter/setterの記載が必要(jp.co.d2c.dnd.entity.tsv)
  public String getCrid() {
    return crid;
  }

  public void setCrid(String crid) {
    this.crid = crid;
  }

  public String getAdm() {
    return adm;
  }

  public void setAdm(String adm) {
    this.adm = adm;
  }

  public String getAdomain() {
    return adomain;
  }

  public void setAdomain(String adomain) {
    this.adomain = adomain;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public String getAttr() {
    return attr;
  }

  public void setAttr(String attr) {
    this.attr = attr;
  }

  public String[] getPositionMapping() {
    return new String[]{"crid", "adm", "adomain", "cat", "attr"};
  }
}
