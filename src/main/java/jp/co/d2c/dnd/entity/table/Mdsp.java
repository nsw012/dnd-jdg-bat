/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class Mdsp implements Serializable {

  private Long id;
  private Long categoryMasterId;
  private String endPoint;
  private boolean invalidFlg;
  private String name;
  private String key256;
  private String key256_2;
  private String cookieSyncUrl;
  private String tag;

}
