package jp.co.d2c.dnd.entity.tsv;

import lombok.Data;
import lombok.experimental.Accessors;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class ImportTsvBaseBanner extends ImportTsvBase {

  private String iurl;
  private String w;
  private String h;
  private String link;

  public String getIurl() {
    return iurl;
  }

  public void setIurl(String iurl) {
    this.iurl = iurl;
  }

  public String getW() {
    return w;
  }

  public void setW(String w) {
    this.w = w;
  }

  public String getH() {
    return h;
  }

  public void setH(String h) {
    this.h = h;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  @Override
  public String[] getPositionMapping() {
    return new String[]{"crid", "adm", "adomain", "cat", "attr", "iurl", "w", "h", "link"};
  }

}
