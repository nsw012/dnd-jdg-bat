/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class MCategory implements Serializable {

  private Long id;
  private Long categoryMasterId;
  private String categoryId;
  private String category;

}
