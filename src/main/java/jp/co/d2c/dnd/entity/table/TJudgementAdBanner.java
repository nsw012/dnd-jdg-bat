/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class TJudgementAdBanner implements Serializable {

  private Long id;
  private Long judgeId;
  private String adm;
  private String iurl;
  private Long w;
  private Long h;
  private String Link;

}
