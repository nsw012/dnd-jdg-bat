package jp.co.d2c.dnd.entity.tsv;

import jp.co.d2c.dnd.constant.JudgementStatus;
import lombok.Data;
import lombok.experimental.Accessors;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class ResultTsvBody {

  private String crid;

  private String tag;

  private JudgementStatus status;

  public String getCrid() {
    return crid;
  }

  public void setCrid(String crid) {
    this.crid = crid;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public JudgementStatus getStatus() {
    return status;
  }

  public void setStatus(JudgementStatus status) {
    this.status = status;
  }

  public void setStatusString(Integer statusString) {
    this.status = JudgementStatus.values()[statusString];
  }

}
