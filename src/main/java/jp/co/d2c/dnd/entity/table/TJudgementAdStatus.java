/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import jp.co.d2c.dnd.constant.JudgementStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class TJudgementAdStatus implements Serializable {

  private Long id;
  private Long judgeId;
  private String tag;
  private JudgementStatus status;

  public void setStatusString(Integer statusString) {
    this.status = JudgementStatus.values()[statusString];
  }

  public Integer getStatusString() {
    return this.status.ordinal();
  }

}
