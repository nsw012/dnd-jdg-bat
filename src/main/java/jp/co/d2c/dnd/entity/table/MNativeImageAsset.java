/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.entity.table;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class MNativeImageAsset implements Serializable {

  private Long id;
  private Long nativeId;
  private Long assetId;
  private Long required;
  private Long type;
  private Long w;
  private Long wmin;
  private Long h;
  private Long hmin;

}
