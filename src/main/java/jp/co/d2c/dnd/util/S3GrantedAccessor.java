package jp.co.d2c.dnd.util;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.Copy;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.Base64;
import com.amazonaws.util.IOUtils;
import org.slf4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * S3へのアクセス機能を提供する
 */
public class S3GrantedAccessor implements S3Accessor {

  private final Logger logger = getLogger(getClass());

  private final AmazonS3 s3;
  private final int retryCount;
  private final int retryInterval;
  private final int preSignedUrlExpireTime;

  public S3GrantedAccessor(AmazonS3 s3, int retryCount, int retryInterval, int preSignedUrlExpireTime) {
    this.s3 = s3;
    this.retryCount = retryCount;
    this.retryInterval = retryInterval;
    this.preSignedUrlExpireTime = preSignedUrlExpireTime;
  }

  private Pattern pattern = Pattern.compile("_(\\d{12})_");

  private String getTime(String filename) {
    Matcher matcher = pattern.matcher(filename);

    if (matcher.find()) {
      return matcher.group();
    } else {
      return "";
    }
  }

  @Override
  public List<String> listFiles(String bucket, String prefix) {
    logger.debug("bucket={},prefix={}", bucket, prefix);
    List<S3ObjectSummary> summaries = new ArrayList<>();

    for (ObjectListing listing = s3.listObjects(bucket, prefix); ; listing = s3.listNextBatchOfObjects(listing)) {
      for (S3ObjectSummary summary : listing.getObjectSummaries()) {
        String key = summary.getKey();

        if (!key.endsWith("/")) {
          summaries.add(summary);
        }
      }

      if (!listing.isTruncated()) {
        break;
      }
    }

    return summaries.stream().sorted(new Comparator<S3ObjectSummary>() {
      @Override
      public int compare(S3ObjectSummary summary1, S3ObjectSummary summary2) {
        return getTime(summary1.getKey()).compareTo(getTime(summary2.getKey()));
      }
    }).map(S3ObjectSummary::getKey).collect(toList());
  }

  private String formatDate(LocalDate date) {
    return DateTimeFormatter.ofPattern("/yyyy/MMdd").format(date);
  }

  @Override
  public List<String> listFiles(String bucket, String prefix, LocalDate date1, LocalDate date2) {
    logger.debug("bucket={},prefix={},date1={},date2={}", bucket, prefix, formatDate(date1), (date2 != null ? formatDate(date2) : "null"));
    LocalDate[] dates = (date2 != null) ? new LocalDate[]{date1, date2} : new LocalDate[]{date1};
    List<S3ObjectSummary> summaries = new ArrayList<>();

    for (LocalDate date : dates) {
      for (ObjectListing listing = s3.listObjects(bucket, prefix + formatDate(date)); ; listing = s3.listNextBatchOfObjects(listing)) {
        for (S3ObjectSummary summary : listing.getObjectSummaries()) {
          String key = summary.getKey();

          if (!key.endsWith("/")) {
            summaries.add(summary);
          }
        }

        if (!listing.isTruncated()) {
          break;
        }
      }
    }

    Optional<S3ObjectSummary> lastSummary = summaries.stream().max(new Comparator<S3ObjectSummary>() {
      @Override
      public int compare(S3ObjectSummary summary1, S3ObjectSummary summary2) {
        return summary1.getLastModified().compareTo(summary2.getLastModified());
      }
    });

    if (lastSummary.isPresent()) {
      Date lastDate = new Date(lastSummary.get().getLastModified().getTime() - 24 * 60 * 60 * 1000);

      return summaries.stream().filter(s -> lastDate.before(s.getLastModified())).sorted(new Comparator<S3ObjectSummary>() {
        @Override
        public int compare(S3ObjectSummary summary1, S3ObjectSummary summary2) {
          return getTime(summary1.getKey()).compareTo(getTime(summary2.getKey()));
        }
      }).map(S3ObjectSummary::getKey).collect(toList());
    } else {
      return new ArrayList<String>();
    }
  }

  @Override
  public void upload(String bucket, String prefix, File file) throws InterruptedException {
    logger.debug("bucket={},prefix={}", bucket, prefix);
    TransferManager tm = new TransferManager(s3);
    try {
      Upload upload = tm.upload(bucket, buildKey(prefix, file.getName()), file);
      upload.waitForCompletion();
    } finally {
      tm.shutdownNow(false);
    }
  }

  @Override
  public void upload(String bucket, String prefix, File file, String contentType) throws IOException, InterruptedException {
    logger.debug("bucket={},prefix={},contentType={}", bucket, prefix, contentType);
    TransferManager tm = new TransferManager(s3);

    try {
      ObjectMetadata objectMetadata = new ObjectMetadata();
      objectMetadata.setContentType(contentType);
      Path path = Paths.get(file.toURI());

      try {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        byte[] buffer = new byte[4096];
        long length = 0;
        int bytes;

        try (InputStream input = Files.newInputStream(path, StandardOpenOption.READ)) {
          while (0 < (bytes = input.read(buffer))) {
            messageDigest.update(buffer, 0, bytes);
            length += bytes;
          }
        }

        String digest = Base64.encodeAsString(messageDigest.digest());
        objectMetadata.setContentMD5(digest);
        objectMetadata.setContentLength(length);
      } catch (NoSuchAlgorithmException e) {
        objectMetadata.setContentLength(Files.size(path));
      }

      try (InputStream input = Files.newInputStream(path, StandardOpenOption.READ)) {
        Upload upload = tm.upload(bucket, buildKey(prefix, file.getName()), input, objectMetadata);
        upload.waitForCompletion();
      }
    } finally {
      tm.shutdownNow(false);
    }
  }

  @Override
  public void upload(S3ObjectName s3ObjectName, MultipartFile multipartFile) throws IOException, InterruptedException {
    logger.debug("bucket={},prefix={}", s3ObjectName.getBucket(), s3ObjectName.getPrefix());
    TransferManager tm = new TransferManager(s3);
    try {
      ObjectMetadata metadata = new ObjectMetadata();
      metadata.setContentLength(IOUtils.toByteArray(multipartFile.getInputStream()).length);
      Upload upload =
        tm.upload(s3ObjectName.getBucket(), buildKey(s3ObjectName.getPrefix(), s3ObjectName.getFileName()), multipartFile.getInputStream(), metadata);
      upload.waitForCompletion();
    } finally {
      tm.shutdownNow(false);
    }
  }

  @Override
  public void download(String bucket, String prefix, File file) throws InterruptedException, FileNotFoundException {

    if (!exist(S3ObjectName.of(bucket, prefix, file.getName()))) {
      throw new FileNotFoundException();
    }
    TransferManager tm = new TransferManager(s3);
    try {
      Download download = tm.download(bucket, buildKey(prefix, file.getName()), file);
      download.waitForCompletion();
    } finally {
      tm.shutdownNow(false);
    }
  }

  @Override
  public void bkup(String bucket, String frmPrefix, String ｆｒｍFile, String toPrefix, String toFile) throws InterruptedException, FileNotFoundException {

    if (!exist(S3ObjectName.of(bucket, frmPrefix, ｆｒｍFile))) {
      throw new FileNotFoundException();
    }
    TransferManager tm = new TransferManager(s3);

    try {
      Copy cp = tm.copy(bucket, buildKey(frmPrefix, ｆｒｍFile), bucket, buildKey(toPrefix, toFile));
      cp.waitForCompletion();
    } finally {
      tm.shutdownNow(false);
    }
  }

  @Override
  public InputStreamResource downloadAsInputStreamResource(String bucket, String prefix, String fileName) throws FileNotFoundException {
    if (!exist(S3ObjectName.of(bucket, prefix, fileName))) {
      throw new FileNotFoundException();
    }
    return new InputStreamResource(s3.getObject(bucket, buildKey(prefix, fileName)).getObjectContent());
  }

  private String buildKey(String prefix, String fileName) {
    final String key = String.join("/", prefix, fileName);
    logger.trace("build key. key={}", key);
    return key;
  }

  @Override
  public void delete(String prefix, String fileName) {
    s3.deleteObject(prefix, fileName);
  }

  @Override
  public void rename(String prefix, String fileName) {
    s3.copyObject(prefix, fileName, prefix, "done_" + fileName);
    delete(prefix, fileName);
  }

  @Override
  public URL getPreSignedUrl(S3ObjectName s3ObjectName) throws FileNotFoundException {

    if (!exist(s3ObjectName)) {
      throw new FileNotFoundException();
    }

    logger.debug("bucket={},prefix={},fileName={}", s3ObjectName.getBucket(), s3ObjectName.getPrefix(), s3ObjectName.getFileName());
    URL url = null;
    try {
      ObjectMetadata s3ObjectMetadata = s3.getObjectMetadata(s3ObjectName.getBucket(), buildKey(s3ObjectName.getPrefix(), s3ObjectName.getFileName()));

      java.util.Date expiration = new java.util.Date();
      long milliSeconds = expiration.getTime() + preSignedUrlExpireTime;
      expiration.setTime(milliSeconds);

      ResponseHeaderOverrides responseHeaders = new ResponseHeaderOverrides();
      responseHeaders.setCacheControl("No-cache");
      responseHeaders.setContentType(s3ObjectMetadata.getContentType());
      s3ObjectName.getDownloadFileName().ifPresent(name -> {
        String encoded;
        try {
          encoded = URLEncoder.encode(name, "UTF-8");
        } catch (UnsupportedEncodingException e) {
          throw new AmazonServiceException("UnsupportedEncodingException");
        }
        responseHeaders.setContentDisposition("attachment; filename*=UTF-8''" + encoded);
      });
      logger.debug("対象ファイルのContent-Type={}", s3ObjectMetadata.getContentType());

      GeneratePresignedUrlRequest generatePresignedUrlRequest =
        new GeneratePresignedUrlRequest(s3ObjectName.getBucket(), buildKey(s3ObjectName.getPrefix(), s3ObjectName.getFileName()));
      generatePresignedUrlRequest.setMethod(HttpMethod.GET);
      generatePresignedUrlRequest.setExpiration(expiration);
      generatePresignedUrlRequest.setResponseHeaders(responseHeaders);

      url = s3.generatePresignedUrl(generatePresignedUrlRequest);

      logger.debug("generatePresignedUrl={}", url.toString());
    } catch (AmazonServiceException exception) {
      logger.error(exception.getMessage());
    }
    return url;
  }

  @Override
  public Boolean exist(S3ObjectName s3ObjectName) {
    int counter = 0;

    while (true) {
      try {
        s3.getObjectMetadata(s3ObjectName.getBucket(), buildKey(s3ObjectName.getPrefix(), s3ObjectName.getFileName()));
        return true;
      } catch (AmazonServiceException e) {
        if (counter++ < retryCount) {
          logger.info(
            MessageFormat.format("ファイルが存在しませんでした。bucket={0},prefix={1},fileName={2}", s3ObjectName.getBucket(), s3ObjectName.getPrefix(),
              s3ObjectName.getFileName()), e);
          logger.info("再試行します");
          try {
            Thread.sleep(retryInterval);
          } catch (Exception ex) {
            // need to do nothing
          }
        } else {
          break;
        }
      }
    }
    return false;
  }

  @Override
  public Boolean existNotRetry(S3ObjectName s3ObjectName) {
    try {
      s3.getObjectMetadata(s3ObjectName.getBucket(), buildKey(s3ObjectName.getPrefix(), s3ObjectName.getFileName()));
      return true;
    } catch (AmazonServiceException e) {
      return false;
    }
  }
}
