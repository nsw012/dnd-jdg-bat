/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class XmlResourceControl extends ResourceBundle.Control {

  public List<String> getFormats(String baseName) {
    if (baseName == null) {
      throw new NullPointerException();
    }

    return Arrays.asList("xml");
  }

  @Override
  public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException,
      InstantiationException, IOException {
    if ((baseName == null) || (locale == null) || (format == null) || (loader == null)) {
      throw new NullPointerException();
    }

    ResourceBundle bundle = null;

    if (format.equals("xml")) {
      String bundleName = toBundleName(baseName, locale);
      String resourceName = toResourceName(bundleName, format);
      InputStream stream = null;

      if (reload) {
        URL url = loader.getResource(resourceName);

        if (url != null) {
          URLConnection connection = url.openConnection();

          if (connection != null) {
            connection.setUseCaches(false);
            stream = connection.getInputStream();
          }
        }
      } else {
        stream = loader.getResourceAsStream(resourceName);
      }

      if (stream != null) {
        BufferedInputStream bis = new BufferedInputStream(stream);
        bundle = new XMLResourceBundle(bis);
        bis.close();
      }
    }

    return bundle;
  }

  private static class XMLResourceBundle extends ResourceBundle {

    private Properties properties;

    public XMLResourceBundle(InputStream stream) throws IOException {
      properties = new Properties();
      properties.loadFromXML(stream);
    }

    public Object handleGetObject(String key) {
      if (key == null) {
        throw new NullPointerException();
      }

      return properties.get(key);
    }

    @SuppressWarnings("unchecked")
    public Enumeration<String> getKeys() {
      return (Enumeration<String>) properties.propertyNames();
    }
  }
}
