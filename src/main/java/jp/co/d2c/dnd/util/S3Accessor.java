package jp.co.d2c.dnd.util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;

/**
 * S3へのアクセス機能を提供する
 */
public interface S3Accessor {

  List<String> listFiles(String bucket, String prefix);

  List<String> listFiles(String bucket, String prefix, LocalDate date1, LocalDate date2);

  void upload(String bucket, String prefix, File file) throws InterruptedException;

  void upload(String bucket, String prefix, File file, String contentType) throws IOException, InterruptedException;

  void upload(S3ObjectName s3ObjectName, MultipartFile multipartFile) throws IOException, InterruptedException;

  void download(String bucket, String prefix, File file) throws InterruptedException, FileNotFoundException;

  InputStreamResource downloadAsInputStreamResource(String bucket, String prefix, String fileName) throws InterruptedException, FileNotFoundException;

  void delete(String prefix, String fileName);

  void rename(String prefix, String fileName);

  URL getPreSignedUrl(S3ObjectName s3ObjectName) throws FileNotFoundException;

  Boolean exist(S3ObjectName s3ObjectName);

  Boolean existNotRetry(S3ObjectName s3ObjectName);

  void bkup(String bucket, String frmPrefix, String ｆｒｍFile, String toPrefix, String toFile) throws InterruptedException, FileNotFoundException;

}
