package jp.co.d2c.dnd.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

  public static String getTodayYMD() {
    return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
  }

  public static String getYesterdayYMD() {
    return LocalDateTime.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyyMMdd"));
  }

}
