package jp.co.d2c.dnd.util;

import java.util.concurrent.atomic.AtomicInteger;

public class HandleCounters {
  private AtomicInteger totalRowCnt;
  private AtomicInteger procRowCnt;
  private AtomicInteger compRowCnt;
  private AtomicInteger deltRowCnt;

  public HandleCounters() {
    totalRowCnt = new AtomicInteger();
    procRowCnt = new AtomicInteger();
    compRowCnt = new AtomicInteger();
    deltRowCnt = new AtomicInteger();
  }

  public void incTotal() {
    totalRowCnt.getAndIncrement();
  }

  public void incProc() {
    procRowCnt.getAndIncrement();
  }

  public void incComp() {
    compRowCnt.getAndIncrement();
  }

  public void incDelt() {
    deltRowCnt.getAndIncrement();
  }

  public int getTotal() {
    return totalRowCnt.get();
  }

  public int getComp() {
    return compRowCnt.get();
  }

  public int getProc() {
    return procRowCnt.get();
  }

  public int getDelt() {
    return deltRowCnt.get();
  }

  public void setTotal(int i) {
    totalRowCnt.set(i);
  }

  public void setProc(int i) {
    procRowCnt.set(i);
  }

  public void setComp(int i) {
    compRowCnt.set(i);
  }

  public void setDelt(int i) {
    deltRowCnt.set(i);
  }

  public void sumTotal(int i) {
    totalRowCnt.set(totalRowCnt.get() + i);
  }

  public void sumProc(int i) {
    procRowCnt.set(procRowCnt.get() + i);
  }

  public void sumComp(int i) {
    compRowCnt.set(compRowCnt.get() + i);
  }

  public void sumDelt(int i) {
    deltRowCnt.set(deltRowCnt.get() + i);
  }
}