/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.util;

import java.time.LocalDateTime;

/**
 * 時刻を提供するクラス
 */
public class DateTimeSupplier {

  public LocalDateTime now() {
    return LocalDateTime.now();
  }
}
