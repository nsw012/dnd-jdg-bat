package jp.co.d2c.dnd.util;

import com.google.common.base.Preconditions;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.validation.constraints.NotNull;

/**
 * S3オブジェクト名を表すクラス
 */
public class S3ObjectName {

  private final Optional<String> bucket;
  private final Optional<String> prefix;
  private final Optional<String> fileName;
  private Optional<String> downloadFileName = Optional.empty();

  public S3ObjectName(Optional<String> bucket, Optional<String> prefix, Optional<String> fileName, LocalDateTime time) {
    Preconditions.checkNotNull(time);
    this.bucket = bucket;
    this.prefix = prefix.map(p -> String.join("/", p, time.format(DateTimeFormatter.ofPattern("yyyy/MMdd"))));
    this.fileName = fileName.map(f -> String.join("_", time.format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")), f));
  }

  public S3ObjectName(Optional<String> bucket, Optional<String> prefix, Optional<String> fileName) {
    this.bucket = bucket;
    this.prefix = prefix;
    this.fileName = fileName;
  }

  public static S3ObjectName of(String bucket, String prefix, String fileName, LocalDateTime time) {
    return new S3ObjectName(Optional.of(bucket), Optional.of(prefix), Optional.of(fileName), time);
  }

  public static S3ObjectName of(String bucket, String prefix, String fileName) {
    return new S3ObjectName(Optional.of(bucket), Optional.of(prefix), Optional.of(fileName));
  }

  public S3ObjectName downloadFileName(@NotNull String downloadFileName) {
    this.downloadFileName = Optional.of(downloadFileName);
    return this;
  }

  public String getBucket() {
    return bucket.orElse("");
  }

  public String getPrefix() {
    return prefix.orElse("");
  }

  public String getFileName() {
    return fileName.orElse("");
  }

  public Optional<String> getDownloadFileName() {
    return downloadFileName;
  }

  public void setDownloadFileName(Optional<String> downloadFileName) {
    this.downloadFileName = downloadFileName;
  }

  public String getKey() {
    return String.join("/", getPrefix(), getFileName());
  }

  public String getName() {
    return StringUtils.replaceEach("s3://:bucket/:prefix/:file_name", //
      new String[] {":bucket", ":prefix", ":file_name"}, //
      new String[] {bucket.orElse(""), prefix.orElse(""), fileName.orElse("")});
  }
}
