package jp.co.d2c.dnd.util;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.bean.BeanToCsv;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;

import java.io.IOException;
import java.util.List;

public class TsvUtil {

  // TODO TSV形式の場合、HeaderColumnNameMappingStrategyにて先頭カラムのマッピングが失敗するためColumnPositionMappingStrategyを使用
  //    HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<T>();
  //    strategy.setType(clazz);

  public static <T extends ImportTsvBase> List<T> getBeans(Class<T> clazz, CSVReader reader) throws Exception {

    ColumnPositionMappingStrategy<T> strategy = new ColumnPositionMappingStrategy<T>();
    strategy.setType(clazz);
    strategy.setColumnMapping(clazz.newInstance().getPositionMapping());

    CsvToBean<T> csv = new CsvToBean<T>();
    return csv.parse(strategy, reader);
  }

  public static <T> List<T> getBeans(Class<T> clazz, CSVReader reader, String[] mappings) throws Exception {

    ColumnPositionMappingStrategy<T> strategy = new ColumnPositionMappingStrategy<T>();
    strategy.setType(clazz);
    strategy.setColumnMapping(mappings);

    CsvToBean<T> csv = new CsvToBean<T>();
    return csv.parse(strategy, reader);
  }

  public static <T> boolean putTsvs(Class<T> clazz, CSVWriter writer, String[] mappings, List<T> data) throws IOException {

    ColumnPositionMappingStrategy<T> mappingStrategy = new ColumnPositionMappingStrategy<T>();
    mappingStrategy.setType(clazz);
    mappingStrategy.setColumnMapping(mappings);

    BeanToCsv<T> csv = new BeanToCsv<T>();
    return csv.write(mappingStrategy, writer, data);
  }

  public static void putSimpleTsvs(CSVWriter writer, List<String[]> data) throws IOException {
    writer.writeAll(data);
  }
}
