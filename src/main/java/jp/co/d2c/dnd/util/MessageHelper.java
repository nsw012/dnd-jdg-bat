/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Stream;

/**
 * メッセージのヘルパクラス
 */
public class MessageHelper {

  private static final Logger logger = LoggerFactory.getLogger(MessageHelper.class);

  private static final String MISSING_RESOURCES = "DNDSM001";
  private static final String CODE_UNDEFINED = "DNDSM002";

  private static final Map<String, String> MESSAGES;

  static {
    final HashMap<String, String> messages = new HashMap<>();
    messages.put(MISSING_RESOURCES, "メッセージリソースが設定されていません");
    messages.put(CODE_UNDEFINED, "メッセージコード {0} は定義されていません");

    MESSAGES = Collections.unmodifiableMap(messages);
  }

  private static final Set<ResourceBundle> RESOURCE_BUNDLES = new HashSet<>();

  private static String buildMessage(String code, String message) {
    return String.join(": ", code, message);
  }

  private static void warning(String code, String message) {
    logger.warn(buildMessage(code, message), new IllegalStateException(message));
  }

  public static void setResourceBundle(ResourceBundle... resourceBundles) {
    Stream.of(resourceBundles).filter(Objects::nonNull).forEach(RESOURCE_BUNDLES::add);
  }

  public static void clearResourceBundle() {
    RESOURCE_BUNDLES.clear();
  }

  public static String getMessage(String code, Object... args) {
    Optional<String> message = Optional.empty();

    if (RESOURCE_BUNDLES.isEmpty()) {
      warning(MISSING_RESOURCES, MESSAGES.get(MISSING_RESOURCES));
    } else {
      message =
          RESOURCE_BUNDLES.stream().filter(resource -> resource.containsKey(code)).findFirst()
              .map(resource -> MessageFormat.format(resource.getString(code), args));

      if (!message.isPresent()) {
        warning(CODE_UNDEFINED, MessageFormat.format(MESSAGES.get(CODE_UNDEFINED), code));
      }
    }

    return buildMessage(code, message.orElse(MessageFormat.format("message not found; args = {0}", Arrays.asList(args))));
  }
}
