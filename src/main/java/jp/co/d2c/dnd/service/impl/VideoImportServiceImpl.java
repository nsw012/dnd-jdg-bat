/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.entity.table.TJudgementAdVideo;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBaseVideo;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.service.VideoImportService;
import jp.co.d2c.dnd.util.HandleCounters;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

/**
 * Video広告取込みサービス
 */
@Service
public class VideoImportServiceImpl extends BaseImportServiceImpl implements VideoImportService {

  @Override
  public List<ImportTsvBaseVideo> importAd() {
    List<ImportTsvBaseVideo> videos;
    try {
      videos = parse(importFile, ImportTsvBaseVideo.class);
      counters.setTotal(videos.size());
      videos.stream()
        .forEach(insert -> {
          counters.incProc();
          try {
            Set<ConstraintViolation<ImportTsvBaseVideo>> results = validator.validate(insert);
            if (!validator.validate(insert).isEmpty()) throw new ConstraintViolationException(results);
            importService.importCreative(insert, tagSupplier(), statusFunction(), insertConsumer());
            counters.incComp();

          } catch (ConstraintViolationException ce) {
            logger.error(getMessage("DNDBM907", importFile.getName(), counters.getProc()));
            logger.error(ce.getConstraintViolations().toString());
          } catch (DndRuntimeException de) {
            logger.error(getMessage(de.getMessage(), importFile.getName(), counters.getProc()));
          }
        });
      return videos;
    } catch (Exception e) {
      logger.error(getMessage("DNDBM908", importFile.getName()));
      throw new DndRuntimeException(e);
    }
  }

  @Override
  public void init(Long dspId, File importFile) {
    this.dspId = dspId;
    this.importFile = importFile;
    this.importService.init(dspId);
    this.counters = new HandleCounters();
    counters.incProc();
  }

  @Override
  public int deleteAd(List<ImportTsvBase> exists) {
    List<String> existIds = exists.stream()
      .map(c -> c.getCrid())
      .collect(Collectors.toList());
    int del = judgeDao.deleteJAdForVideo(dspId, existIds);
    counters.setDelt(del);
    return del;
  }

  private BiConsumer<ImportTsvBaseVideo, Long> insertConsumer() {
    return (insert, finalAdId) -> {
      TJudgementAdVideo adv = new TJudgementAdVideo();
      adv.setJudgeId(finalAdId);
      adv.setAdm(insert.getAdm());
      adv.setProtocol(insert.getProtocol());
      adv.setH(NumberUtils.toLong(insert.getH()));
      adv.setW(NumberUtils.toLong(insert.getW()));
      adv.setLink((insert.getLink()));
      judgeDao.insertJAdVi(adv);
    };
  }

  private Function<String, Collection<String>> tagSupplier() {
    return (crId) -> {
      Optional<Collection<String>> dstTags = judgeDao.selectRegisteredTags(crId, dspId);
      Optional<Collection<String>> orgTags = impDao.selectVideoTags();
      return CollectionUtils.subtract(orgTags.orElse(ListUtils.EMPTY_LIST), dstTags.orElse(ListUtils.EMPTY_LIST));
    };
  }

  private BiFunction<String, String, JudgementStatus> statusFunction() {
    return (tag, adm) -> JudgementStatus.UNEXAMINED;
  }
}
