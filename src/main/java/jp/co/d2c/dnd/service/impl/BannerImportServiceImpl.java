/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.entity.table.TJudgementAdBanner;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBaseBanner;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.service.BannerImportService;
import jp.co.d2c.dnd.util.HandleCounters;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;


/**
 * Banner広告取込みサービス
 */
@Service
public class BannerImportServiceImpl extends BaseImportServiceImpl implements BannerImportService {

  @Override
  public List<ImportTsvBaseBanner> importAd() {
    List<ImportTsvBaseBanner> banners;
    try {
      banners = parse(importFile, ImportTsvBaseBanner.class);
      counters.setTotal(banners.size());
      banners.stream()
        .forEach((ImportTsvBaseBanner insert) -> {
          counters.incProc();
          try {
            Set<ConstraintViolation<ImportTsvBaseBanner>> results = validator.validate(insert);
            if (!validator.validate(insert).isEmpty()) throw new ConstraintViolationException(results);
            importService.importCreative(insert, tagSupplier(), statusFunction(), insertConsumer());
            counters.incComp();

          } catch (ConstraintViolationException ce) {
            logger.error(getMessage("DNDBM907", importFile.getName(), counters.getProc()));
            logger.error(ce.getConstraintViolations().toString());
          } catch (DndRuntimeException de) {
            logger.error(getMessage(de.getMessage(), importFile.getName(), counters.getProc()));
          }
        });
      return banners;
    } catch (Exception e) {
      logger.error(getMessage("DNDBM908", importFile.getName()));
      throw new DndRuntimeException(e);
    }
  }

  @Override
  public void init(Long dspId, File importFile) {
    this.dspId = dspId;
    this.importFile = importFile;
    this.importService.init(dspId);
    this.counters = new HandleCounters();
    counters.incProc();
  }

  @Override
  public int deleteAd(List<ImportTsvBase> exists) {
    List<String> existIds = exists.stream()
      .map(c -> c.getCrid())
      .collect(Collectors.toList());
    int del = judgeDao.deleteJAdForBanner(dspId, existIds);
    counters.setDelt(del);
    return del;
  }

  private BiConsumer<ImportTsvBaseBanner, Long> insertConsumer() {
    return (insert, finalAdId) -> {
      TJudgementAdBanner adb = new TJudgementAdBanner();
      adb.setJudgeId(finalAdId);
      adb.setAdm(insert.getAdm());
      adb.setH(NumberUtils.toLong(insert.getH()));
      adb.setW(NumberUtils.toLong(insert.getW()));
      adb.setIurl(insert.getIurl());
      adb.setLink((insert.getLink()));
      judgeDao.insertJAdBu(adb);
    };
  }

  private Function<String, Collection<String>> tagSupplier() {
    return (crId) -> {
      Optional<Collection<String>> dstTags = judgeDao.selectRegisteredTags(crId, dspId);
      Optional<Collection<String>> orgTags = impDao.selectBannerTags();
      return CollectionUtils.subtract(orgTags.orElse(ListUtils.EMPTY_LIST), dstTags.orElse(ListUtils.EMPTY_LIST));
    };
  }

  private BiFunction<String, String, JudgementStatus> statusFunction() {
    return (tag, adm) -> JudgementStatus.UNEXAMINED;
  }
}
