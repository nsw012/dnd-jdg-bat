/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service;

import jp.co.d2c.dnd.entity.table.TJudgementAdStatus;
import jp.co.d2c.dnd.util.HandleCounters;

import java.util.List;

/**
 * Banner自動審査サービス
 */
public interface ExaminService {

  void init(Long dspId);

  void examination(List<TJudgementAdStatus> judgeList);

  void examinBanner();

  void examinNative();

  void examinVideo();

  HandleCounters getCounters();
}
