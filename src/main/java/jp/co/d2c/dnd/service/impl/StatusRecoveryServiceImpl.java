/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import javassist.*;
import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.entity.table.TJudgementAd;
import jp.co.d2c.dnd.entity.table.TJudgementAdStatus;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.service.StatusRecoveryService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

/**
 * 審査ステータス復旧サービス
 */
@Service
public class StatusRecoveryServiceImpl extends BaseImportServiceImpl implements StatusRecoveryService {

  @Override
  public void recovery() {
    List<Object> entities;
    List<String> header;
    try {
      header = parseHeader(importFile);

      CtClass cc = createEntityClass(
        RandomStringUtils.randomAlphabetic(5),
        header,
        "crid");

      entities = parse(importFile, cc.toClass(), header.toArray(new String[0]));

      entities.stream()
        .forEach(e -> {
          try {
            Method getCrId = e.getClass().getMethod("getcrid");
            TJudgementAd judgeAd = judgeDao.selectJAd((String) getCrId.invoke(e), dspId).get();

            List<Method> getEatchTagStatus = Arrays.stream(e.getClass().getDeclaredMethods())
              .filter(method -> !method.getName().equals("getcrid") && method.getName().startsWith("get"))
              .collect(Collectors.toList());

            getEatchTagStatus.stream()
              .forEach(getTag -> {
                try {
                  TJudgementAdStatus update = new TJudgementAdStatus();
                  update.setJudgeId(judgeAd.getId());
                  String tagName = StringUtils.remove(getTag.getName(), "get");
                  update.setTag(tagName);
                  update.setStatus(JudgementStatus.values()[Integer.valueOf((String) getTag.invoke(e))]);
                  judgeDao.updateJAdStStatus(update);
                } catch (Exception exp) {
                  throw new DndRuntimeException(exp);
                }
              });
          } catch (Exception exp) {
            throw new DndRuntimeException(exp);
          }
        });
    } catch (Exception e) {
      logger.error(getMessage("DNDBM999", importFile.getName()));
      throw new DndRuntimeException(e);
    }
  }

  @Override
  public void init(Long dspId, File importFile) {
    this.dspId = dspId;
    this.importFile = importFile;
    this.importService.init(dspId);
  }

  private CtClass createEntityClass(String className,
                                    List<String> headerNames,
                                    String ignoreColumn)
    throws Exception {

    // class create
    CtClass cc = ClassPool.getDefault().makeClass(className);

    // common column
    CtField crfield = CtField.make("private String crid;", cc);
    cc.addField(crfield);
    CtMethod crgetter = CtNewMethod.make("public String getcrid(){return crid;}", cc);
    CtMethod crsetter = CtNewMethod.make("public void setcrid(String ex){this.crid=ex;}", cc);
    cc.addMethod(crgetter);
    cc.addMethod(crsetter);

    // create tag column to class
    headerNames.stream()
      .filter(f -> !f.equals(ignoreColumn))
      .forEach(m -> {
        try {
          CtField field = CtField.make(String.join("", "private String ", m, ";"), cc);
          cc.addField(field);
          CtMethod getter = CtNewMethod.make(
            String.join("", "public String get", m, "() {return ", m, ";}"), cc);
          cc.addMethod(getter);
          CtMethod setter = CtNewMethod.make(
            String.join("", "public void set", m, "(String ex) {this.", m, "=ex;}"), cc);
          cc.addMethod(setter);
        } catch (Exception e) {
          e.printStackTrace();
        }
      });

    return cc;
  }
}
