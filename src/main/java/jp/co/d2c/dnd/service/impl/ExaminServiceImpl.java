/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.dao.BlockCategoryDao;
import jp.co.d2c.dnd.dao.BlockDomainDao;
import jp.co.d2c.dnd.dao.ImpressionDao;
import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.entity.table.MImp;
import jp.co.d2c.dnd.entity.table.TJudgementAdStatus;
import jp.co.d2c.dnd.service.ExaminService;
import jp.co.d2c.dnd.util.HandleCounters;
import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

/**
 * 自動審査サービス
 */
@Service
public class ExaminServiceImpl implements ExaminService {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private JudgementDao judgeDao;

  @Autowired
  private ImpressionDao impDao;

  @Autowired
  private BlockCategoryDao bCategoryDao;

  @Autowired
  private BlockDomainDao bDomainDao;

  protected HandleCounters counters;

  private Long dspId;

  @Override
  public void examinBanner() {
    List<TJudgementAdStatus> judgeList = judgeDao.selectJudgeListBn(dspId);
    examination(judgeList);
    counters.setTotal(judgeList.size());
  }

  @Override
  public void examinNative() {
    List<TJudgementAdStatus> judgeList = judgeDao.selectJudgeListNa(dspId);
    examination(judgeList);
    counters.setTotal(judgeList.size());
  }

  @Override
  public void examinVideo() {
    List<TJudgementAdStatus> judgeList = judgeDao.selectJudgeListVi(dspId);
    examination(judgeList);
    counters.setTotal(judgeList.size());
  }

  @Override
  public void examination(List<TJudgementAdStatus> judgeList) {

    judgeList.stream().forEach(j -> {

      MImp imp = impDao.selectByTag(j.getTag());
      if (!Optional.ofNullable(imp).isPresent()) {
        logger.warn(getMessage("DNDBM306", j.getJudgeId().toString(), j.getTag()));
        return;
      }

      List<String> domains = judgeDao.selectJudgeDomains(j.getJudgeId());
      List<String> bDomains = bDomainDao.selectDomainsByAdPageId(imp.getAdPageId());
      Optional domDeny = ListUtils.intersection(domains, bDomains).stream().findFirst();
      if (domDeny.isPresent()) {
        logger.warn(getMessage("DNDBM304", j.getJudgeId().toString(), j.getTag(), domDeny.get()));
        return;
      }

      List<String> categories = judgeDao.selectJudgeCategories(j.getJudgeId());
      List<String> bCategories = bCategoryDao.selectCategoriesByAdPageId(imp.getAdPageId());
      Optional catDeny = ListUtils.intersection(categories, bCategories).stream().findFirst();
      if (catDeny.isPresent()) {
        logger.warn(getMessage("DNDBM305", j.getJudgeId().toString(), j.getTag(), catDeny.get()));
        return;
      }

      judgeDao.updateJAdStStatus(j.setStatus(JudgementStatus.PRE_APPROVED));
    });
  }

  @Override
  public void init(Long dspId) {
    this.dspId = dspId;
    this.counters = new HandleCounters();
  }

  public HandleCounters getCounters() {
    return counters;
  }
}
