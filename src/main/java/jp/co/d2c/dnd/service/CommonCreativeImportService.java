package jp.co.d2c.dnd.service;

import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Common広告取込みサービス
 */
public interface CommonCreativeImportService {

  void init(Long dspId);

  <TSV extends ImportTsvBase> void importCreative(TSV tsv,
                                                  Function<String, Collection<String>> tagSupplier,
                                                  BiFunction<String, String, JudgementStatus> status,
                                                  BiConsumer<TSV, Long> insertAdType);

}
