/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;
import jp.co.d2c.dnd.dao.ImpressionDao;
import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.service.CommonCreativeImportService;
import jp.co.d2c.dnd.util.HandleCounters;
import jp.co.d2c.dnd.util.TsvUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Validation;
import javax.validation.Validator;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

/**
 * 広告取込みサービス
 */
@Service
public class BaseImportServiceImpl {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  protected JudgementDao judgeDao;

  @Autowired
  protected ImpressionDao impDao;

  @Autowired
  protected CommonCreativeImportService importService;

  protected HandleCounters counters;

  protected File importFile;

  protected Long dspId;

  protected final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

  protected <TS extends ImportTsvBase> List<TS> parse(File file, Class<TS> clazz) throws Exception {
    CSVReader reader = new CSVReader(new FileReader(file), '\t', CSVParser.DEFAULT_QUOTE_CHARACTER, 1);
    return TsvUtil.getBeans(clazz, reader);
  }

  protected List parse(File file, Class clazz, String[] mappings) throws Exception {
    CSVReader reader = new CSVReader(new FileReader(file), '\t', CSVParser.DEFAULT_QUOTE_CHARACTER, 1);
    return TsvUtil.getBeans(clazz, reader, mappings);
  }

  protected List<String> parseHeader(File file) throws Exception {
    CSVReader reader = new CSVReader(new FileReader(file), '\t', CSVParser.DEFAULT_QUOTE_CHARACTER);
    return Arrays.asList(reader.readNext());
  }

  public HandleCounters getCounters() {
    return counters;
  }
}
