/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service;

import java.util.Optional;

/**
 * 広告取込ディレクトリサービス
 */
public interface ImportDirService {

  void registImportDate(Long dspId, String latestImportDir);

  Optional<String> getImportDate(Long dspId);

}
