/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.dao.CategoryDao;
import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.entity.table.*;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.service.CommonCreativeImportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;


/**
 * Common広告取込みサービス
 */
@Service
public class CommonCreativeImportServiceImpl implements CommonCreativeImportService {

  @Autowired
  private JudgementDao judgeDao;

  @Autowired
  private CategoryDao categoryDao;

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  private Long dspId;

  @Override
  @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
  public <TSV extends ImportTsvBase> void importCreative(TSV tsv,
                                                         Function<String, Collection<String>> tagSupplier,
                                                         BiFunction<String, String, JudgementStatus> statusFunction,
                                                         BiConsumer<TSV, Long> insertConsumer) {

    // t_judgement_ad
    TJudgementAd ad = judgeDao.selectJAd(tsv.getCrid(), dspId)
      .orElseGet(() -> {
        TJudgementAd newAd = new TJudgementAd();
        newAd.setCrId(tsv.getCrid());
        newAd.setDspId(dspId);
        TJudgementAd inserted = judgeDao.insertAndSelectJAd(newAd).get();

        // t_judgement_ad_banner/native/video
        insertConsumer.accept(tsv, inserted.getId());

        // t_judgement_ad_category
        Arrays.stream(tsv.getCat().split(","))
          .forEach(str -> {
            // category_id
            MCategory category = categoryDao.selectByCategoryId(str)
              .orElseThrow(() -> new DndRuntimeException("DNDBM909"));
            TJudgementAdCategory adc = new TJudgementAdCategory();
            adc.setJudgeId(inserted.getId());
            adc.setCategoryId(category.getId());
            judgeDao.insertJAdCa(adc);
          });

        // t_judgement_ad_domain
        Arrays.stream(tsv.getAdomain().split(","))
          .forEach(str -> {
            TJudgementAdDomain add = new TJudgementAdDomain();
            add.setJudgeId(inserted.getId());
            add.setAdomain(str);
            judgeDao.insertJAdDo(add);
          });

        // t_judgement_ad_attribute
        Arrays.stream(tsv.getAttr().split(","))
          .forEach(str -> {
            TJudgementAdAttribute ada = new TJudgementAdAttribute();
            ada.setJudgeId(inserted.getId());
            ada.setValue(str);
            judgeDao.insertJAdAt(ada);
          });

        return inserted;
      });

    // t_judgement_ad_status
    Collection<String> tags = tagSupplier.apply(ad.getCrId());
    tags.stream()
      .filter(tag -> !judgeDao.existJAdSt(ad.getId(), tag))
      .forEach(tag -> {
        TJudgementAdStatus ads = new TJudgementAdStatus();
        ads.setJudgeId(ad.getId());
        ads.setStatus(statusFunction.apply(tag, tsv.getAdm()));
        ads.setTag(tag);
        judgeDao.insertJAdSt(ads);
      });
  }

  @Override
  public void init(Long dspId) {
    this.dspId = dspId;
  }
}
