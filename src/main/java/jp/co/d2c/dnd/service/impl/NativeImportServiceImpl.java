/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.google.openrtb.OpenRtb;
import com.google.openrtb.json.OpenRtbJsonFactory;
import com.google.protobuf.UninitializedMessageException;
import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.entity.table.MNativeImageAsset;
import jp.co.d2c.dnd.entity.table.TJudgementAdNative;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBaseNative;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.service.NativeImportService;
import jp.co.d2c.dnd.util.HandleCounters;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;


/**
 * Native広告取込みサービス
 */
@Service
public class NativeImportServiceImpl extends BaseImportServiceImpl implements NativeImportService {

  @Override
  public List<ImportTsvBaseNative> importAd() {
    List<ImportTsvBaseNative> natives;
    try {
      natives = parse(importFile, ImportTsvBaseNative.class);
      counters.setTotal(natives.size());
      natives.stream()
        .forEach(insert -> {
          counters.incProc();
          try {
            Set<ConstraintViolation<ImportTsvBaseNative>> results = validator.validate(insert);
            if (!validator.validate(insert).isEmpty()) throw new ConstraintViolationException(results);
            importService.importCreative(insert, tagSupplier(), statusFunction(), insertConsumer());
            counters.incComp();

          } catch (ConstraintViolationException ce) {
            logger.error(getMessage("DNDBM907", importFile.getName(), counters.getProc()));
            logger.error(ce.getConstraintViolations().toString());
          } catch (DndRuntimeException de) {
            logger.error(getMessage(de.getMessage(), importFile.getName(), counters.getProc()));
            Optional.ofNullable(de.getCause())
              .ifPresent(e -> logger.error(e.getMessage(), importFile.getName(), counters.getProc()));
            ;
          }
        });
      return natives;
    } catch (Exception e) {
      logger.error(getMessage("DNDBM908", importFile.getName()));
      throw new DndRuntimeException(e);
    }
  }

  @Override
  public void init(Long dspId, File importFile) {
    this.dspId = dspId;
    this.importFile = importFile;
    this.importService.init(dspId);
    this.counters = new HandleCounters();
    counters.incProc();
  }

  @Override
  public int deleteAd(List<ImportTsvBase> exists) {
    List<String> existIds = exists.stream()
      .map(c -> c.getCrid())
      .collect(Collectors.toList());
    int del = judgeDao.deleteJAdForNative(dspId, existIds);
    counters.setDelt(del);
    return del;
  }

  private BiConsumer<ImportTsvBaseNative, Long> insertConsumer() {
    return (insert, finalAdId) -> {
      TJudgementAdNative adn = new TJudgementAdNative();
      adn.setJudgeId(finalAdId);
      adn.setAdm(insert.getAdm());
      adn.setTitle(insert.getTitle());
      adn.setImg(insert.getImg());
      adn.setSponsor(insert.getSponsor());
      adn.setOptout(insert.getOptout());
      adn.setLink((insert.getLink()));
      judgeDao.insertJAdNa(adn);
    };
  }

  private Function<String, Collection<String>> tagSupplier() {
    return (crId) -> {
      Optional<Collection<String>> dstTags = judgeDao.selectRegisteredTags(crId, dspId);
      Optional<Collection<String>> orgTags = impDao.selectNativeTags();
      return CollectionUtils.subtract(orgTags.orElse(ListUtils.EMPTY_LIST), dstTags.orElse(ListUtils.EMPTY_LIST));
    };
  }

  private BiFunction<String, String, JudgementStatus> statusFunction() {
    return (tag, adm) -> {
      Optional<MNativeImageAsset> asset = impDao.selectNativeImageAssetByTag(tag);
      if (asset.isPresent()) {
        Predicate<Optional<MNativeImageAsset>> imgRequire = x -> x.get().getRequired() == 1;
        Predicate<String> containImgUrl = x -> containImgUrl(x);
        return imgRequire.test(asset) && !containImgUrl.test(adm) ?
          JudgementStatus.EXCLUDED : JudgementStatus.UNEXAMINED;
      }
      return JudgementStatus.UNEXAMINED;
    };
  }

  private boolean containImgUrl(String adm) {

    OpenRtbJsonFactory openRtbJsonFactory =
      OpenRtbJsonFactory.create().setJsonFactory(new JsonFactory());

    try {
      OpenRtb.NativeResponse.Builder builder = openRtbJsonFactory.newNativeReader()
        .readNativeResponse(openRtbJsonFactory.getJsonFactory().createParser(adm));

      List<OpenRtb.NativeResponse.Asset> assetsList = builder.getAssetsList();
      Optional.ofNullable(assetsList)
        .filter(a -> a.isEmpty())
        .ifPresent(e -> {
          throw new DndRuntimeException("DNDBM911", new Exception("assets undefined"));
        });

      long count = assetsList.stream()
        .filter(f -> !StringUtils.isEmpty(f.getImg().getUrl()))
        .count();

      return count > 0;

    } catch (IOException ioe) {
      throw new DndRuntimeException("DNDBM911", ioe);
    } catch (UninitializedMessageException ume) {
      throw new DndRuntimeException("DNDBM911", ume);
    }
  }
}
