/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import au.com.bytecode.opencsv.CSVWriter;
import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.dao.ImpressionDao;
import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.entity.tsv.ResultTsvBody;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.util.HandleCounters;
import jp.co.d2c.dnd.util.TsvUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.util.*;


/**
 * 広告審査結果出力サービス
 */
@Service
public class BaseExportServiceImpl {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  protected JudgementDao judgeDao;

  @Autowired
  protected ImpressionDao impDao;

  protected HandleCounters counters;

  protected File exportFile;

  protected Long dspId;

  private final static String TOP_COLUMN = "crid";

  protected void output(File file, Collection<String> header, Collection<ResultTsvBody> bodys) {
    try {
      CSVWriter writer = new CSVWriter(new FileWriter(file), '\t', CSVWriter.NO_QUOTE_CHARACTER);

      // Header
      List<String> headerList = new ArrayList<>();
      headerList.add(TOP_COLUMN);
      headerList.addAll(header);

      List<String[]> headerArray = new ArrayList<>();
      headerArray.add(headerList.toArray(new String[0]));

      // Body
      Map<String, Map<String, JudgementStatus>> bodyMap = new HashMap<>();
      bodys.stream().forEach(b -> {
        if (bodyMap.containsKey(b.getCrid())) {
          bodyMap.get(b.getCrid()).put(b.getTag(), b.getStatus());
        } else {
          bodyMap.put(b.getCrid(), new HashMap() {{
            put(b.getTag(), b.getStatus());
          }});
        }
      });

      List<String[]> bodyArray = new ArrayList<>();
      bodyMap.entrySet().stream().forEach(b -> {
        List<String> line = new ArrayList();
        line.add(b.getKey());
        headerList.stream().filter(f -> !f.equals(TOP_COLUMN)).forEach(h -> {
          line.add(String.valueOf(Optional.ofNullable(b.getValue().get(h))
            .orElse(JudgementStatus.UNEXAMINED).ordinal()));
        });
        bodyArray.add(line.toArray(new String[0]));
      });

      TsvUtil.putSimpleTsvs(writer, headerArray);
      TsvUtil.putSimpleTsvs(writer, bodyArray);

      writer.flush();
      writer.close();

    } catch (Exception e) {
      e.printStackTrace();
      throw new DndRuntimeException(e);
    }
  }

  public HandleCounters getCounters() {
    return counters;
  }

}
