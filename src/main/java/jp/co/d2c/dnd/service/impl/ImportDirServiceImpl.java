/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.entity.table.TJudgementAdImpDir;
import jp.co.d2c.dnd.service.ImportDirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * 広告取込ディレクトリサービス
 */
@Service
public class ImportDirServiceImpl implements ImportDirService {

  @Autowired
  private JudgementDao judgeDao;

  public void registImportDate(Long dspId, String latestImportDir) {

    TJudgementAdImpDir regist = new TJudgementAdImpDir();
    regist.setDspId(dspId);
    regist.setLatestImportDir(latestImportDir);

    if (Optional.ofNullable(judgeDao.selectJAId(dspId)).isPresent()) {
      judgeDao.updateJAId(regist);
    } else {
      judgeDao.insertJAId(regist);
    }

  }

  @Override
  public Optional<String> getImportDate(Long dspId) {
    TJudgementAdImpDir select = judgeDao.selectJAId(dspId);
    if (Optional.ofNullable(select).isPresent()) {
      return Optional.ofNullable(select.getLatestImportDir());
    }
    return Optional.empty();
  }
}
