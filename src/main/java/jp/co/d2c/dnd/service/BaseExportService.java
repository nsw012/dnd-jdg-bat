/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service;

import jp.co.d2c.dnd.util.HandleCounters;

import java.io.File;

public interface BaseExportService {

  void init(Long dspId, File exportFile);

  void exportAd();

  HandleCounters getCounters();
}
