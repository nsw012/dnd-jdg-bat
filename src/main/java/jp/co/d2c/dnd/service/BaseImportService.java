/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service;

import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.util.HandleCounters;

import java.io.File;
import java.util.List;

public interface BaseImportService {

  void init(Long dspId, File importFile);

  List<? extends ImportTsvBase> importAd();

  int deleteAd(List<ImportTsvBase> existIds);

  HandleCounters getCounters();
}
