/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.entity.tsv.ResultTsvBody;
import jp.co.d2c.dnd.service.VideoExportService;
import jp.co.d2c.dnd.util.HandleCounters;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;
import java.util.Optional;


/**
 * Video広告審査結果出力サービス
 */
@Service
public class VideoExportServiceImpl extends BaseExportServiceImpl implements VideoExportService {

  @Override
  public void exportAd() {
    Optional<Collection<String>> header = impDao.selectVideoTags();
    Optional<Collection<ResultTsvBody>> body = judgeDao.selectResultBodyVi(dspId);
    header.ifPresent(h -> body.ifPresent(b -> output(exportFile, h, b)));
    header.ifPresent(h -> body.ifPresent(b -> counters.setTotal(b.size() / h.size())));
  }

  @Override
  public void init(Long dspId, File exportFile) {
    this.dspId = dspId;
    this.exportFile = exportFile;
    this.counters = new HandleCounters();
  }
}
