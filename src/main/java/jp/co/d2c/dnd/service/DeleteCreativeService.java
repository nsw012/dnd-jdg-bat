/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service;

import java.util.List;

/**
 * クリエイティブ削除サービス
 */
public interface DeleteCreativeService {

  void init(Long dspId);

  void deleteAd(List<String> existIds);

  boolean isPresentOtherID(List<String> existIds);

}
