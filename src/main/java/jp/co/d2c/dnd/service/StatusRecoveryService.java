/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service;

import jp.co.d2c.dnd.entity.tsv.ResultTsvBody;

import java.io.File;
import java.util.List;

/**
 * 審査ステータス復旧サービス
 */
public interface StatusRecoveryService {

  void init(Long dspId, File importFile);

  void recovery();

}
