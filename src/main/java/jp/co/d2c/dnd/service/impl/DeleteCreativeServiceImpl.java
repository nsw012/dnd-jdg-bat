/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.service.DeleteCreativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * クリエイティブ削除サービス
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
public class DeleteCreativeServiceImpl implements DeleteCreativeService {

  @Autowired
  private JudgementDao judgeDao;

  private Long dspId;

  @Override
  public void init(Long dspId) {
    this.dspId = dspId;
  }

  @Override
  public void deleteAd(List<String> existIds) {
    judgeDao.deleteJAd(dspId, existIds);
  }

  @Override
  public boolean isPresentOtherID(List<String> existIds) {
    if (!existIds.isEmpty()) {
      return judgeDao.isNotInJad(dspId, existIds);
    }
    else {
      return false;
    }
  }
}
