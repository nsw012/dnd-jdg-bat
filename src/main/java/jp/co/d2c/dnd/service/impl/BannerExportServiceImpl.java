/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.service.impl;

import jp.co.d2c.dnd.entity.tsv.ResultTsvBody;
import jp.co.d2c.dnd.service.BannerExportService;
import jp.co.d2c.dnd.util.HandleCounters;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;
import java.util.Optional;


/**
 * Banner広告審査結果出力サービス
 */
@Service
public class BannerExportServiceImpl extends BaseExportServiceImpl implements BannerExportService {

  @Override
  public void exportAd() {
    Optional<Collection<String>> header = impDao.selectBannerTags();
    Optional<Collection<ResultTsvBody>> body = judgeDao.selectResultBodyBn(dspId);
    header.ifPresent(h -> body.ifPresent(b -> output(exportFile, h, b)));
    header.ifPresent(h -> body.ifPresent(b -> counters.setTotal(b.size() / h.size())));
  }

  @Override
  public void init(Long dspId, File exportFile) {
    this.dspId = dspId;
    this.exportFile = exportFile;
    this.counters = new HandleCounters();
  }
}
