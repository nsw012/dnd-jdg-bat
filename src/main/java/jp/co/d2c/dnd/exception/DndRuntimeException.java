/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.exception;

/**
 * バッチ実行時の例外クラス
 */
@SuppressWarnings("serial")
public class DndRuntimeException extends RuntimeException {

  public DndRuntimeException(String message) {
    super(message);
  }

  public DndRuntimeException(Exception cause) {
    super(cause);
  }

  public DndRuntimeException(String message, Exception cause) {
    super(message, cause);
  }
}
