/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao;

import jp.co.d2c.dnd.entity.table.MCategory;
import jp.co.d2c.dnd.entity.table.MImp;

import java.util.Collection;
import java.util.Optional;

/**
 * m_category
 */
public interface CategoryDao {

  boolean existsById(String id);

  int insert(MImp insert);

  Optional<MCategory> selectById(Long id);

  Optional<MCategory> selectByCategoryId(String categoryId);

  Collection<MCategory> selectAllTags();

}
