/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.impl;

import jp.co.d2c.dnd.dao.ImpressionDao;
import jp.co.d2c.dnd.dao.base.BaseDao;
import jp.co.d2c.dnd.entity.table.MImp;
import jp.co.d2c.dnd.entity.table.MNativeImageAsset;
import org.apache.commons.collections.ListUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


/**
 * m_imp
 */
@Component
public class ImpressionDaoImpl extends BaseDao implements ImpressionDao {

  @Override
  public boolean existsById(Long id) {
    return false;
  }

  @Override
  public int insert(MImp insert) {
    return 0;
  }

  @Override
  public MImp selectById(Long id) {
    return null;
  }

  @Override
  public Collection<MImp> selectAllTags() {
    try {
      return namedParameterJdbcTemplate
        .query(
          "SELECT * FROM m_imp",
          newMapSqlParameterSource(), resultSetExtractor);
    } catch (EmptyResultDataAccessException e) {
      return ListUtils.EMPTY_LIST;
    }
  }

  @Override
  public Optional<Collection<String>> selectBannerTags() {
    try {
      return Optional.ofNullable(namedParameterJdbcTemplate
        .queryForList(
          "SELECT tag FROM m_imp where banner_id is not NULL",
          newMapSqlParameterSource(), String.class));
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Collection<String>> selectNativeTags() {
    try {
      return Optional.ofNullable(namedParameterJdbcTemplate
        .queryForList(
          "SELECT tag FROM m_imp where native_id is not NULL",
          newMapSqlParameterSource(), String.class));
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Collection<String>> selectVideoTags() {
    try {
      return Optional.ofNullable(namedParameterJdbcTemplate
        .queryForList(
          "SELECT tag FROM m_imp where video_id is not NULL",
          newMapSqlParameterSource(), String.class));
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }


  @Override
  public MImp selectByTag(String tag) {
    try {
      return namedParameterJdbcTemplate.queryForObject("SELECT * FROM m_imp WHERE tag = :tag;",
        newMapSqlParameterSource().addValue("tag", tag),
        newRowMapper(MImp.class));
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Optional<MNativeImageAsset> selectNativeImageAssetByTag(String tag) {
    try {
      return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(
        "SELECT asset.* " +
          " FROM m_imp imp, m_native_image_asset asset" +
          " WHERE imp.native_id = asset.native_id " +
          " AND imp.tag = :tag;",
        newMapSqlParameterSource().addValue("tag", tag),
        newRowMapper(MNativeImageAsset.class)));
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  private ResultSetExtractor<List<MImp>> resultSetExtractor = (ResultSet results) -> {
    List<MImp> imps = new ArrayList<MImp>();

    while (results.next()) {
      MImp imp = new MImp();
      imp.setId(results.getLong("id"));
      imp.setAdPageId(results.getLong("ad_page_id"));
      imp.setFloorPrice(results.getBigDecimal("floor_price"));
      imp.setSecure(results.getBoolean("secure"));
      imp.setBannerId(results.getLong("banner_id"));
      imp.setNativeId(results.getLong("native_id"));
      imp.setVideoId(results.getLong("video_id"));
      imp.setTag(results.getString("tag"));
      imps.add(imp);
    }
    return imps.isEmpty() ? null : imps;
  };

}
