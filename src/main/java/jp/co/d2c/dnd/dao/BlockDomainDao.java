/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao;

import jp.co.d2c.dnd.entity.table.MAdPageBlockDomain;

import java.util.List;

/**
 * m_ad_page_block_domain
 */
public interface BlockDomainDao {

  MAdPageBlockDomain selectById(Long id);

  MAdPageBlockDomain selectByAdPageId(Long adPageId);

  List<String> selectDomainsByAdPageId(Long adPageId);

}
