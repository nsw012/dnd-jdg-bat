/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.base;

import static java.nio.charset.StandardCharsets.UTF_8;

import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.jdbc.DndPropertyRowMapper;
import jp.co.d2c.dnd.jdbc.DndMapSqlParameterSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.io.InputStreamReader;

/**
 * テーブル操作の基底クラス
 */
public class BaseDao {

  @Autowired
  @Qualifier("namedParameterJdbcTemplate")
  protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  protected String loadSql(String name) {
    try (InputStreamReader reader = new InputStreamReader(BaseDao.class.getResourceAsStream(name), UTF_8)) {
      StringBuilder sql = new StringBuilder();
      char[] buf = new char[4096];
      int length;

      while (0 < (length = reader.read(buf))) {
        sql.append(new String(buf, 0, length));
      }

      return sql.toString();
    } catch (Exception e) {
      throw new DndRuntimeException(e);
    }
  }

  protected org.springframework.jdbc.core.namedparam.MapSqlParameterSource newMapSqlParameterSource() {
    return new DndMapSqlParameterSource();
  }

  protected org.springframework.jdbc.core.namedparam.MapSqlParameterSource newMapSqlParameterSource(String palamName, Object value) {
    return newMapSqlParameterSource().addValue(palamName, value);
  }

  protected <T> RowMapper<T> newRowMapper(Class<T> mappedClass) {
    return new DndPropertyRowMapper<>(mappedClass);
  }
}
