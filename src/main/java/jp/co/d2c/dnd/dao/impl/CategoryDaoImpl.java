/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.impl;

import jp.co.d2c.dnd.dao.CategoryDao;
import jp.co.d2c.dnd.dao.base.BaseDao;
import jp.co.d2c.dnd.entity.table.MCategory;
import jp.co.d2c.dnd.entity.table.MImp;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

/**
 * m_imp
 */
@Component
public class CategoryDaoImpl extends BaseDao implements CategoryDao {

  @Override
  public boolean existsById(String id) {
    return false;
  }

  @Override
  public int insert(MImp insert) {
    return 0;
  }

  @Override
  public Optional<MCategory> selectById(Long id) {
    return Optional.empty();
  }

  @Override
  public Optional<MCategory> selectByCategoryId(String categoryId) {
    try {
      return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(
        "SELECT * FROM m_category WHERE category_id = :category_id;",
        newMapSqlParameterSource().addValue("category_id", categoryId),
        newRowMapper(MCategory.class)));
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Collection<MCategory> selectAllTags() {
    return null;
  }

}
