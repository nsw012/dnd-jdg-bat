/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao;

import jp.co.d2c.dnd.entity.table.Mdsp;

/**
 * m_dsp
 */
public interface DspDao {

  boolean existsById(String id);

  int insert(Mdsp insert);

  Mdsp selectById(Long id);

  Mdsp selectByTag(String tag);

}
