/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.impl;

import jp.co.d2c.dnd.constant.JudgementStatus;
import jp.co.d2c.dnd.dao.JudgementDao;
import jp.co.d2c.dnd.dao.base.BaseDao;
import jp.co.d2c.dnd.entity.table.*;
import jp.co.d2c.dnd.entity.tsv.ResultTsvBody;
import jp.co.d2c.dnd.jdbc.BeanPropertySqlParameterSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * t_judgement_XXX
 */
@Component
public class JudgementDaoImpl extends BaseDao implements JudgementDao {

  @Override
  public boolean existJAd(String crId, Long dspId) {
    try {
      Integer count =
        namedParameterJdbcTemplate
          .queryForObject(
            "SELECT COUNT(*) FROM t_judgement_ad WHERE cr_id = :cr_id AND dsp_id = :dsp_id",
            newMapSqlParameterSource()
              .addValue("cr_id", crId)
              .addValue("dsp_id", dspId),
            Integer.class);
      return count.intValue() > 0;
    } catch (EmptyResultDataAccessException e) {
      return false;
    }
  }

  @Override
  public Optional<TJudgementAd> insertAndSelectJAd(TJudgementAd insert) {
    insertJAd(insert);
    return selectJAd(insert.getCrId(), insert.getDspId());
  }

  @Override
  public int insertJAd(TJudgementAd insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad (cr_id, dsp_id)"
            + " VALUES (:cr_id, :dsp_id)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public Optional<TJudgementAd> selectJAd(String crId, Long dspId) {
    try {
      return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(
        "SELECT * FROM t_judgement_ad WHERE cr_id = :cr_id AND dsp_id = :dsp_id",
        newMapSqlParameterSource()
          .addValue("cr_id", crId)
          .addValue("dsp_id", dspId),
        newRowMapper(TJudgementAd.class)));

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public int deleteJAd(Long dspId, List<String> exist) {
    return
      namedParameterJdbcTemplate
        .update(
          "DELETE FROM t_judgement_ad where dsp_id = :dsp_id AND cr_id NOT IN (:cr_id)",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("cr_id", exist));
  }

  @Override
  public int deleteJAdForBanner(Long dspId, List<String> exist) {
    return
      namedParameterJdbcTemplate
        .update(
          "DELETE FROM t_judgement_ad WHERE dsp_id = :dsp_id AND cr_id NOT IN (:cr_id)" +
            " AND EXISTS (SELECT 1 FROM t_judgement_ad_banner ba WHERE t_judgement_ad.id = ba.judge_id)",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("cr_id", exist));
  }

  @Override
  public int deleteJAdForNative(Long dspId, List<String> exist) {
    return
      namedParameterJdbcTemplate
        .update(
          "DELETE FROM t_judgement_ad WHERE dsp_id = :dsp_id AND cr_id NOT IN (:cr_id)" +
            " AND EXISTS (SELECT 1 FROM t_judgement_ad_native na WHERE t_judgement_ad.id = na.judge_id)",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("cr_id", exist));
  }

  @Override
  public int deleteJAdForVideo(Long dspId, List<String> exist) {
    return
      namedParameterJdbcTemplate
        .update(
          "DELETE FROM t_judgement_ad WHERE dsp_id = :dsp_id AND cr_id NOT IN (:cr_id)" +
            " AND EXISTS (SELECT 1 FROM t_judgement_ad_video vi WHERE t_judgement_ad.id = vi.judge_id)",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("cr_id", exist));
  }

  @Override
  public boolean isNotInJad(Long dspId, List<String> exist) {
    try {
      Integer count =
        namedParameterJdbcTemplate
          .queryForObject(
            "SELECT count(*) FROM t_judgement_ad WHERE dsp_id = :dsp_id AND cr_id NOT IN (:cr_id)",
            newMapSqlParameterSource()
              .addValue("dsp_id", dspId)
              .addValue("cr_id", exist),
            Integer.class);
      return count.intValue() > 0;
    } catch (EmptyResultDataAccessException e) {
      return false;
    }
  }

  @Override
  public int insertJAdBu(TJudgementAdBanner insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_banner (judge_id, adm, iurl, w, h, link)"
            + " VALUES (:judge_id, :adm, :iurl, :w, :h, :link)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public int insertJAdCa(TJudgementAdCategory insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_category (judge_id, category_id)"
            + " VALUES (:judge_id, :category_id)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public int insertJAdDo(TJudgementAdDomain insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_domain (judge_id, adomain)"
            + " VALUES (:judge_id, :adomain)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public int insertJAdAt(TJudgementAdAttribute insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_attribute (judge_id, value)"
            + " VALUES (:judge_id, :value)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public int insertJAdNa(TJudgementAdNative insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_native (judge_id, adm, title, img, sponsor, optout, link)"
            + " VALUES (:judge_id, :adm, :title, :img, :sponsor, :optout, :link)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public boolean existJAdSt(Long judgeId, String tag) {
    try {
      Integer count =
        namedParameterJdbcTemplate
          .queryForObject(
            "SELECT COUNT(*) FROM t_judgement_ad_status WHERE judge_id = :judge_id AND tag = :tag",
            newMapSqlParameterSource()
              .addValue("judge_id", judgeId)
              .addValue("tag", tag),
            Integer.class);
      return count.intValue() > 0;
    } catch (EmptyResultDataAccessException e) {
      return false;
    }

  }

  @Override
  public int insertJAdSt(TJudgementAdStatus insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_status (judge_id, tag, status)"
            + " VALUES (:judge_id, :tag, :status_string)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public int insertJAdVi(TJudgementAdVideo insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_video (judge_id, adm, protocol, w, h, link)"
            + " VALUES (:judge_id, :adm, :protocol, :w, :h, :link)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public Optional<Collection<ResultTsvBody>> selectResultBodyBn(Long dspId) {
    List<ResultTsvBody> rtn =
      namedParameterJdbcTemplate
        .query(
          "SELECT " +
            "ad.cr_id AS crid, " +
            "st.tag AS tag, " +
            "st.status AS status_string " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st, " +
            "t_judgement_ad_banner bn " +
            "WHERE ad.id = st.judge_id AND ad.id = bn.judge_id AND ad.dsp_id = :dsp_id " +
            "ORDER by ad.cr_id",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId),
          newRowMapper(ResultTsvBody.class));
    return rtn.isEmpty() ? Optional.empty() : Optional.ofNullable(rtn);
  }

  @Override
  public Optional<Collection<ResultTsvBody>> selectResultBodyNa(Long dspId) {
    List<ResultTsvBody> rtn =
      namedParameterJdbcTemplate
        .query(
          "SELECT " +
            "ad.cr_id AS crid, " +
            "st.tag AS tag, " +
            "st.status AS status_string " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st, " +
            "t_judgement_ad_native na " +
            "WHERE ad.id = st.judge_id AND ad.id = na.judge_id AND ad.dsp_id = :dsp_id " +
            "ORDER by ad.cr_id",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId),
          newRowMapper(ResultTsvBody.class));
    return rtn.isEmpty() ? Optional.empty() : Optional.ofNullable(rtn);
  }

  @Override
  public Optional<Collection<ResultTsvBody>> selectResultBodyVi(Long dspId) {
    List<ResultTsvBody> rtn =
      namedParameterJdbcTemplate
        .query(
          "SELECT " +
            "ad.cr_id AS crid, " +
            "st.tag AS tag, " +
            "st.status AS status_string " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st, " +
            "t_judgement_ad_video vi " +
            "WHERE ad.id = st.judge_id AND ad.id = vi.judge_id AND ad.dsp_id = :dsp_id " +
            "ORDER by ad.cr_id",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId),
          newRowMapper(ResultTsvBody.class));
    return rtn.isEmpty() ? Optional.empty() : Optional.ofNullable(rtn);
  }

  @Override
  public Optional<Collection<String>> selectRegisteredTags(String crid, Long dspId) {
    List<String> rtn =
      namedParameterJdbcTemplate
        .queryForList(
          "SELECT " +
            "st.tag " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st " +
            "WHERE ad.id = st.judge_id AND ad.cr_id = :cr_id AND ad.dsp_id = :dsp_id",
          newMapSqlParameterSource()
            .addValue("cr_id", crid)
            .addValue("dsp_id", dspId),
          String.class);
    return rtn.isEmpty() ? Optional.empty() : Optional.ofNullable(rtn);
  }

  @Override
  public TJudgementAdImpDir selectJAId(Long dspId) {
    try {
      return namedParameterJdbcTemplate.queryForObject(
        "SELECT * FROM t_judgement_ad_impdir WHERE dsp_id = :dsp_id",
        newMapSqlParameterSource()
          .addValue("dsp_id", dspId),
        newRowMapper(TJudgementAdImpDir.class));
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public int insertJAId(TJudgementAdImpDir insert) {
    return
      namedParameterJdbcTemplate
        .update(
          "INSERT INTO t_judgement_ad_impdir (dsp_id, latest_import_dir)"
            + " VALUES (:dsp_id, :latest_import_dir)",
          new BeanPropertySqlParameterSource(insert));
  }

  @Override
  public int updateJAId(TJudgementAdImpDir update) {
    return
      namedParameterJdbcTemplate
        .update(
          "UPDATE t_judgement_ad_impdir set latest_import_dir = :latest_import_dir"
            + " WHERE dsp_id = :dsp_id",
          new BeanPropertySqlParameterSource(update));
  }

  @Override
  public List<TJudgementAdStatus> selectJudgeListBn(Long dspId) {
    List<TJudgementAdStatus> rtn =
      namedParameterJdbcTemplate
        .query(
          "SELECT " +
            "st.id AS id, " +
            "st.judge_id AS judge_id, " +
            "st.tag AS tag, " +
            "st.status AS status_string " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st, " +
            "t_judgement_ad_banner bn " +
            "WHERE ad.id = st.judge_id AND ad.id = bn.judge_id " +
            "AND ad.dsp_id = :dsp_id AND st.status = :status ",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("status", JudgementStatus.UNEXAMINED.ordinal()),
          newRowMapper(TJudgementAdStatus.class));
    return rtn;
  }

  @Override
  public List<TJudgementAdStatus> selectJudgeListNa(Long dspId) {
    List<TJudgementAdStatus> rtn =
      namedParameterJdbcTemplate
        .query(
          "SELECT " +
            "st.id AS id, " +
            "st.judge_id AS judge_id, " +
            "st.tag AS tag, " +
            "st.status AS status_string " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st, " +
            "t_judgement_ad_native na " +
            "WHERE ad.id = st.judge_id AND ad.id = na.judge_id " +
            "AND ad.dsp_id = :dsp_id AND st.status = :status ",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("status", JudgementStatus.UNEXAMINED.ordinal()),
          newRowMapper(TJudgementAdStatus.class));
    return rtn;
  }

  @Override
  public List<TJudgementAdStatus> selectJudgeListVi(Long dspId) {
    List<TJudgementAdStatus> rtn =
      namedParameterJdbcTemplate
        .query(
          "SELECT " +
            "st.id AS id, " +
            "st.judge_id AS judge_id, " +
            "st.tag AS tag, " +
            "st.status AS status_string " +
            "FROM " +
            "t_judgement_ad ad, " +
            "t_judgement_ad_status st, " +
            "t_judgement_ad_video vi " +
            "WHERE ad.id = st.judge_id AND ad.id = vi.judge_id " +
            "AND ad.dsp_id = :dsp_id AND st.status = :status ",
          newMapSqlParameterSource()
            .addValue("dsp_id", dspId)
            .addValue("status", JudgementStatus.UNEXAMINED.ordinal()),
          newRowMapper(TJudgementAdStatus.class));
    return rtn;
  }

  @Override
  public List<String> selectJudgeDomains(Long judgeId) {
    List<String> rtn =
      namedParameterJdbcTemplate
        .queryForList(
          "SELECT " +
            "do.adomain AS domain " +
            "FROM " +
            "t_judgement_ad_domain do " +
            "WHERE do.judge_id = :judge_id ",
          newMapSqlParameterSource()
            .addValue("judge_id", judgeId),
          String.class);
    return rtn;
  }

  @Override
  public List<String> selectJudgeCategories(Long judgeId) {
    List<String> rtn =
      namedParameterJdbcTemplate
        .queryForList(
          "SELECT " +
            "dc.category_id AS category " +
            "FROM " +
            "t_judgement_ad_category dc " +
            "WHERE dc.judge_id = :judge_id ",
          newMapSqlParameterSource()
            .addValue("judge_id", judgeId),
          String.class);
    return rtn;
  }

  @Override
  public int updateJAdStStatus(TJudgementAdStatus update) {
    BeanPropertySqlParameterSource a = new BeanPropertySqlParameterSource(update);

    return
      namedParameterJdbcTemplate
        .update(
          "UPDATE t_judgement_ad_status set status = :status_string"
            + " WHERE judge_id = :judge_id"
            + " AND tag = :tag",
          new BeanPropertySqlParameterSource(update));
  }

}
