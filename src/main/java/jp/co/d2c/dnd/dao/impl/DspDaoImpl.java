/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.impl;

import jp.co.d2c.dnd.dao.base.BaseDao;
import jp.co.d2c.dnd.dao.DspDao;
import jp.co.d2c.dnd.entity.table.Mdsp;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;


/**
 * m_dsp
 */
@Component
public class DspDaoImpl extends BaseDao implements DspDao {

  @Override
  public boolean existsById(String id) {
    return false;
  }

  @Override
  public int insert(Mdsp insert) {
    return 0;
  }

  @Override
  public Mdsp selectById(Long id) {
    return null;
  }

  @Override
  public Mdsp selectByTag(String tag) {
    try {
      return namedParameterJdbcTemplate.queryForObject("SELECT * FROM m_dsp WHERE tag = :tag;",
        newMapSqlParameterSource().addValue("tag", tag),
        newRowMapper(Mdsp.class));
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }
}
