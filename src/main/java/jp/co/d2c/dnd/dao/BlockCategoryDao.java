/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao;

import jp.co.d2c.dnd.entity.table.MAdPageBlockCategory;

import java.util.List;

/**
 * m_ad_page_block_category
 */
public interface BlockCategoryDao {

  MAdPageBlockCategory selectById(Long id);

  MAdPageBlockCategory selectByAdPageId(String adPageId);

  List<String> selectCategoriesByAdPageId(Long adPageId);

}
