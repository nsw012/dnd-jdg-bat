/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao;

import jp.co.d2c.dnd.entity.table.*;
import jp.co.d2c.dnd.entity.tsv.ResultTsvBody;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * t_judgement_XXX
 */
public interface JudgementDao {

  boolean existJAd(String crId, Long dspId);

  Optional<TJudgementAd> selectJAd(String crId, Long dspId);

  Optional<TJudgementAd> insertAndSelectJAd(TJudgementAd insert);

  int insertJAd(TJudgementAd insert);

  int deleteJAd(Long dspId, List<String> exist);

  boolean isNotInJad(Long dspId, List<String> exist);

  int insertJAdSt(TJudgementAdStatus insert);

  int updateJAdStStatus(TJudgementAdStatus update);

  boolean existJAdSt(Long judgeId, String tag);

  int insertJAdBu(TJudgementAdBanner insert);

  int insertJAdNa(TJudgementAdNative insert);

  int insertJAdVi(TJudgementAdVideo insert);

  int insertJAdAt(TJudgementAdAttribute insert);

  int insertJAdCa(TJudgementAdCategory insert);

  int insertJAdDo(TJudgementAdDomain insert);

  TJudgementAdImpDir selectJAId(Long dspId);

  int insertJAId(TJudgementAdImpDir insert);

  int updateJAId(TJudgementAdImpDir update);

  int deleteJAdForBanner(Long dspId, List<String> exist);

  int deleteJAdForNative(Long dspId, List<String> exist);

  int deleteJAdForVideo(Long dspId, List<String> exist);

  Optional<Collection<ResultTsvBody>> selectResultBodyBn(Long dspId);

  Optional<Collection<ResultTsvBody>> selectResultBodyNa(Long dspId);

  Optional<Collection<ResultTsvBody>> selectResultBodyVi(Long dspId);

  Optional<Collection<String>> selectRegisteredTags(String crid, Long dspId);

  List<TJudgementAdStatus> selectJudgeListBn(Long dspId);

  List<TJudgementAdStatus> selectJudgeListNa(Long dspId);

  List<TJudgementAdStatus> selectJudgeListVi(Long dspId);

  List<String> selectJudgeDomains(Long judgeId);

  List<String> selectJudgeCategories(Long judgeId);
}
