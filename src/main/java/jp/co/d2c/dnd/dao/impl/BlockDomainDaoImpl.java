/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.impl;

import jp.co.d2c.dnd.dao.BlockDomainDao;
import jp.co.d2c.dnd.dao.base.BaseDao;
import jp.co.d2c.dnd.entity.table.MAdPageBlockDomain;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * m_ad_page_block_domain
 */
@Component
public class BlockDomainDaoImpl extends BaseDao implements BlockDomainDao {

  @Override
  public MAdPageBlockDomain selectById(Long id) {
    return null;
  }

  @Override
  public MAdPageBlockDomain selectByAdPageId(Long adPageId) {
    try {
      return namedParameterJdbcTemplate.queryForObject("SELECT * FROM m_ad_page_block_domain WHERE ad_page_id = :ad_page_id;",
        newMapSqlParameterSource().addValue("ad_page_id", adPageId),
        newRowMapper(MAdPageBlockDomain.class));
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public List<String> selectDomainsByAdPageId(Long adPageId) {
    try {
      return namedParameterJdbcTemplate.queryForList("SELECT domain FROM m_ad_page_block_domain WHERE ad_page_id = :ad_page_id;",
        newMapSqlParameterSource().addValue("ad_page_id", adPageId),
        String.class);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

}
