/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao;

import jp.co.d2c.dnd.entity.table.MImp;
import jp.co.d2c.dnd.entity.table.MNativeImageAsset;

import java.util.Collection;
import java.util.Optional;

/**
 * m_imp
 */
public interface ImpressionDao {

  boolean existsById(Long id);

  int insert(MImp insert);

  MImp selectById(Long id);

  MImp selectByTag(String tag);

  Collection<MImp> selectAllTags();

  Optional<Collection<String>> selectBannerTags();

  Optional<Collection<String>> selectNativeTags();

  Optional<Collection<String>> selectVideoTags();

  Optional<MNativeImageAsset> selectNativeImageAssetByTag(String tag);

}
