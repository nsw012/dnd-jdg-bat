/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.dao.impl;

import jp.co.d2c.dnd.dao.BlockCategoryDao;
import jp.co.d2c.dnd.dao.base.BaseDao;
import jp.co.d2c.dnd.entity.table.MAdPageBlockCategory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * m_ad_page_block_category
 */
@Component
public class BlockCategoryDaoImpl extends BaseDao implements BlockCategoryDao {

  @Override
  public MAdPageBlockCategory selectById(Long id) {
    return null;
  }

  @Override
  public MAdPageBlockCategory selectByAdPageId(String adPageId) {
    try {
      return namedParameterJdbcTemplate.queryForObject("SELECT * FROM m_ad_page_block_category WHERE ad_page_id = :ad_page_id;",
        newMapSqlParameterSource().addValue("ad_page_id", adPageId),
        newRowMapper(MAdPageBlockCategory.class));
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public List<String> selectCategoriesByAdPageId(Long adPageId) {
    try {
      return namedParameterJdbcTemplate.queryForList("SELECT category_id FROM m_ad_page_block_category WHERE ad_page_id = :ad_page_id;",
        newMapSqlParameterSource().addValue("ad_page_id", adPageId),
        String.class);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

}