package jp.co.d2c.dnd.batch.impl;

import com.google.common.io.Files;
import jp.co.d2c.dnd.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;


/**
 * 審査結果出力バッチ
 */
@Component("DNDJDGEXP")
public class DndJdgExportBatch extends DndJdgBatch {

  @Autowired
  private BannerExportService bannerService;

  @Autowired
  private NativeExportService nativeService;

  @Autowired
  private VideoExportService videoService;

  @Autowired
  private ImportDirService impDirService;

  private Optional<String> targetDateDir = Optional.empty();

  @PostConstruct
  public void init() {
    s3Bucket = environment.getProperty("aws.s3.bucket");
  }

  @Override
  protected void doInTemplate() {

    parameter();
    List<String> outputs = adTypes.stream().map(e -> {
      if (e.equals("banner")) return environment.getProperty("judge.result.file.banner");
      if (e.equals("native")) return environment.getProperty("judge.result.file.native");
      if (e.equals("video")) return environment.getProperty("judge.result.file.video");
      return null;
    }).collect(Collectors.toList());

    logger.info(getMessage("DNDBM200", dspName));

    checkTargetDate();

    targetDateDir.ifPresent(date -> {
      outputs.stream()
        .filter(i -> Optional.ofNullable(i).isPresent())
        .forEach(f -> {
          logger.info(getMessage("DNDBM201", f));
          try {
            final File dir = Files.createTempDir();
            final File output = new File(dir.getPath(), f);

            // Banner
            Optional.ofNullable(f)
              .filter(ff -> ff.equals(environment.getProperty("judge.result.file.banner")))
              .ifPresent(fff -> exportAdService(output,bannerService));

            // Native
            Optional.ofNullable(f)
              .filter(ff -> ff.equals(environment.getProperty("judge.result.file.native")))
              .ifPresent(fff -> exportAdService(output,nativeService));

            // Video
            Optional.ofNullable(f)
              .filter(ff -> ff.equals(environment.getProperty("judge.result.file.video")))
              .ifPresent(fff -> exportAdService(output,videoService));

            logger.info(getMessage("DNDBM202", f));
            final String prefix = String.join("/", dspName, targetDateDir.get());
            final String bkPrefix = String.join("/", dspName,
              LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
            uploadS3(s3Bucket, output, prefix);
            bkupS3(s3Bucket, output, prefix, bkPrefix);

          } catch (Exception e) {
            logger.info(getMessage("DNDBM802", f));
            e.printStackTrace();
          }
        });
      logger.info(getMessage("DNDBM206", dspName));
    });
  }


  private void exportAdService(File output, BaseExportService service) {
    service.init(dspId, output);
    service.exportAd();
    logger.info(getMessage("DNDBM207",service.getCounters().getTotal()));
  }

  private void uploadS3(String s3Bucket, File file, String s3Prefix) throws Exception {
    try {
      if (file.exists()) s3Accessor.upload(s3Bucket, s3Prefix, file);
    } catch (Exception e) {
      logger.warn(getMessage("DNDBM902", file.getName()), e);
      throw new Exception(e);
    }
  }

  private void checkTargetDate() {
    targetDateDir = impDirService.getImportDate(dspId);

    logger.info(getMessage("DNDBM109",
      targetDateDir.isPresent() ? targetDateDir.get() : "対象ディレクトリおよびファイルが存在しません"));
  }

}