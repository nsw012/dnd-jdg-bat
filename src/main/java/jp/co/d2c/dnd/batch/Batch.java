/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.batch;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * バッチが実装するインターフェース
 */
public interface Batch {

  default String getBatchCode() {
    return getClass().getAnnotation(Component.class).value();
  }

  default void afterExecute() {
    // do nothing
  }

  void execute(List<String> parameters) throws Exception;
}
