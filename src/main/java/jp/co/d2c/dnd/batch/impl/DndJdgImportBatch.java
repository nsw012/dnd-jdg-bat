package jp.co.d2c.dnd.batch.impl;

import com.google.common.io.Files;
import jp.co.d2c.dnd.entity.tsv.ImportTsvBase;
import jp.co.d2c.dnd.service.*;
import jp.co.d2c.dnd.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

/**
 * 審査広告取込バッチ
 */
@Component("DNDJDGIMP")
public class DndJdgImportBatch extends DndJdgBatch {

  @Autowired
  private BannerImportService bannerService;

  @Autowired
  private NativeImportService nativeService;

  @Autowired
  private VideoImportService videoService;

  @Autowired
  private ImportDirService impDirService;

  private Optional<String> targetDateDir = Optional.empty();

  @PostConstruct
  public void init() {
    s3Bucket = environment.getProperty("aws.s3.bucket");
  }

  @Override
  protected void doInTemplate() {
    parameter();
    List<String> inputs = adTypes.stream().map(e -> {
      if (e.equals("banner")) return environment.getProperty("judge.file.banner");
      if (e.equals("native")) return environment.getProperty("judge.file.native");
      if (e.equals("video")) return environment.getProperty("judge.file.video");
      return null;
    }).collect(Collectors.toList());
    logger.info(getMessage("DNDBM100", dspName));

    checkTargetDate(s3Bucket, dspName, inputs);

    targetDateDir.ifPresent(date -> {
      inputs.stream()
        .filter(i -> Optional.ofNullable(i).isPresent())
        .forEach(f -> {
          logger.info(getMessage("DNDBM101", f));
          try {
            final File dir = Files.createTempDir();
            final File input = new File(dir, f);
            final String prefix = String.join("/", dspName, targetDateDir.get());
            final String bkPrefix = String.join("/", dspName,
              LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
            downloadS3(s3Bucket, input, prefix);
            bkupS3(s3Bucket, input, prefix, bkPrefix);

            // Banner
            Optional.ofNullable(f)
              .filter(fa -> fa.equals(environment.getProperty("judge.file.banner")))
              .filter(fb -> input.exists())
              .ifPresent(fc -> importAdService(input, bannerService));

            // Native
            Optional.ofNullable(f)
              .filter(fa -> fa.equals(environment.getProperty("judge.file.native")))
              .filter(fb -> input.exists())
              .ifPresent(fc -> importAdService(input, nativeService));

            // Video
            Optional.ofNullable(f)
              .filter(fa -> fa.equals(environment.getProperty("judge.file.video")))
              .filter(fb -> input.exists())
              .ifPresent(fc -> importAdService(input, videoService));

          } catch (Exception e) {
            logger.error(getMessage("DNDBM801", f));
            e.printStackTrace();
          }
        });

      impDirService.registImportDate(dspId, targetDateDir.get());

      logger.info(getMessage("DNDBM106", dspName));
    });
  }

  private void importAdService(File input, BaseImportService service) {
    List<ImportTsvBase> creatives = new ArrayList<ImportTsvBase>();

    service.init(dspId, input);
    creatives.addAll(service.importAd());

    logger.info(getMessage("DNDBM107",
      service.getCounters().getComp(),
      service.getCounters().getTotal()));

    service.deleteAd(creatives);
    logger.info(getMessage("DNDBM104",
      service.getCounters().getDelt()));
  }

  private void checkTargetDate(String s3Bucket, String s3Prefix, List<String> targetList) {
    List<String> todayFiles = s3Accessor
      .listFiles(s3Bucket, String.join("/", s3Prefix, DateUtil.getTodayYMD()))
      .stream()
      .map(f -> new File(f).getName())
      .collect(Collectors.toList());

    List<String> yesterDayFiles = s3Accessor
      .listFiles(s3Bucket, String.join("/", s3Prefix, DateUtil.getYesterdayYMD()))
      .stream()
      .map(f -> new File(f).getName())
      .collect(Collectors.toList());

    targetList.stream().forEach(t -> {
      if (yesterDayFiles.contains(t)) {
        targetDateDir = Optional.of(DateUtil.getYesterdayYMD());
        return;
      }
    });

    targetList.stream().forEach(t -> {
      if (todayFiles.contains(t)) {
        targetDateDir = Optional.of(DateUtil.getTodayYMD());
        return;
      }
    });

    logger.info(getMessage("DNDBM109",
      targetDateDir.isPresent() ? targetDateDir.get() : "対象ディレクトリおよびファイルが存在しません"));

  }
}
