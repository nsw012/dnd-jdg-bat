package jp.co.d2c.dnd.batch.impl;

import com.google.common.io.Files;
import jp.co.d2c.dnd.service.StatusRecoveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

/**
 * 審査ステータス復旧バッチ
 */
@Component("DNDJDGRCV")
public class DndJdgRecoveryBatch extends DndJdgBatch {

  @Autowired
  private StatusRecoveryService recoveryService;

  @PostConstruct
  public void init() {
    s3Bucket = environment.getProperty("aws.s3.bucket");
  }

  @Override
  protected void doInTemplate() {
    parameter();
    List<String> inputs = adTypes.stream().map(e -> {
      if (e.equals("banner")) return environment.getProperty("judge.result.file.banner");
      if (e.equals("native")) return environment.getProperty("judge.result.file.native");
      if (e.equals("video")) return environment.getProperty("judge.result.file.video");
      return null;
    }).collect(Collectors.toList());
    logger.info(getMessage("DNDBM400", dspName));

    inputs.stream()
      .filter(i -> Optional.ofNullable(i).isPresent())
      .forEach(f -> {

        logger.info(getMessage("DNDBM401", f));
        try {
          final File dir = Files.createTempDir();
          final File input = new File(dir, f);
          final String rcvPrefix = String.join("", "recovery/", dspName);

          downloadS3(s3Bucket, input, rcvPrefix);

          Optional.ofNullable(f)
            .filter(fb -> input.exists())
            .ifPresent(fc -> {
              recoveryService.init(dspId, input);
              recoveryService.recovery();
              logger.info(getMessage("DNDBM404", f));
            });

        } catch (Exception e) {
          logger.error(getMessage("DNDBM804", f));
          e.printStackTrace();
        }
      });

    logger.info(getMessage("DNDBM403", dspName));
  }

}
