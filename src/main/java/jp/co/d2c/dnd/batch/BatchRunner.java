/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.batch;

import static java.util.Arrays.asList;
import static java.util.Arrays.copyOfRange;

import org.slf4j.MDC;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.lang.management.ManagementFactory;

/**
 * バッチを起動するアプリケーションクラス
 */
public class BatchRunner {

  private static final int EXIT_NORMAL = 0;

  private static final int EXIT_FAILED = 1;

  @SuppressWarnings("rawtypes")
  public static void main(String[] args) {
    int exitStatus;

    try (AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext()) {
      Class appConfig = Class.forName("jp.co.d2c.dnd.springconfig.AppConfig");
      Class serviceConfig = Class.forName("jp.co.d2c.dnd.springconfig.ServiceConfig");
      Class dataAccessConfig = Class.forName("jp.co.d2c.dnd.springconfig.DataAccessConfig");
      Class awsConfig = Class.forName("jp.co.d2c.dnd.springconfig.AwsConfig");

      applicationContext.register(appConfig, serviceConfig, dataAccessConfig, awsConfig);
      applicationContext.refresh();

      MDC.put("batch_code", args[0]);
      MDC.put("host_name", args[1]);
      MDC.put("pid", ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);

      Batch batch = applicationContext.getBean(args[0], Batch.class);
      batch.execute(asList(copyOfRange(args, 2, args.length)));
      exitStatus = EXIT_NORMAL;
    } catch (Throwable t) {
      t.printStackTrace();
      exitStatus = EXIT_FAILED;
    }

    System.exit(exitStatus);
  }
}
