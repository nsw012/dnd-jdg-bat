/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.batch;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;

import java.util.Collections;
import java.util.List;

/**
 * バッチを実装するための抽象クラス
 */
public abstract class BatchBase implements Batch {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private static final int RUN_COMPLETED = 0;

  private static final int RUN_FAILED = 1;

  private List<String> parameters;

  private long lastChecked;

  private final long waitPrepare;

  public BatchBase(long waitPrepare) {
    this.waitPrepare = waitPrepare;
  }

  public BatchBase() {
    this(60 * 1000);
  }

  protected final List<String> getParameters() {
    return parameters;
  }

  protected boolean checkStop(long interval) {
    if (interval < (System.currentTimeMillis() - lastChecked)) {
      lastChecked = System.currentTimeMillis();
    }

    return false;
  }

  protected boolean checkStop() {
    return checkStop(0);
  }

  @Override
  public void execute(List<String> parameters) throws Exception {
    this.parameters = Collections.unmodifiableList(parameters);
    lastChecked = System.currentTimeMillis();

    String batchCode = getBatchCode();

    try {
        logger.info(getMessage("DNDBM001", parameters));
        int status = RUN_FAILED;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
          doInExecute();

          status = RUN_COMPLETED;
        } finally {
          try {
            afterExecute();
          } catch (Throwable t) {
            logger.warn(getMessage("DNDBM007", t.getLocalizedMessage()), t);
          }

          stopWatch.stop();
          logger.info(getMessage("DNDBM003", status, stopWatch.getTotalTimeSeconds()));

        }
    } catch (Throwable t) {
      logger.error(getMessage("DNDBM004", t.getLocalizedMessage()), t);
      throw t;
    }
  }

  protected abstract void doInExecute() throws Exception;
}
