/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.batch.impl;

import jp.co.d2c.dnd.batch.NonResidentBatchTemplate;
import jp.co.d2c.dnd.dao.DspDao;
import jp.co.d2c.dnd.exception.DndRuntimeException;
import jp.co.d2c.dnd.util.DateTimeSupplier;
import jp.co.d2c.dnd.util.S3Accessor;
import jp.co.d2c.dnd.util.S3ObjectName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;

/**
 * donald審査バッチ抽象クラス
 */
public abstract class DndJdgBatch extends NonResidentBatchTemplate {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  protected Environment environment;

  @Autowired
  protected DateTimeSupplier dateTimeSupplier;

  @Autowired
  protected S3Accessor s3Accessor;

  @Autowired
  private DspDao DspDao;

  protected Long dspId;
  protected String dspName;
  protected List<String> adTypes;
  protected String s3Bucket;

  public DndJdgBatch() {
    super(60 * 1000);
  }

  protected void requestStop() {
  }

  protected void parameter() {

    List<String> parameters = getParameters();

    try {
      dspName = parameters.get(0);
      Optional.of(DspDao.selectByTag(dspName)).ifPresent(d -> dspId = d.getId());
    } catch (ArrayIndexOutOfBoundsException e) {
      logger.error(getMessage("DNDBM905"));
      throw new DndRuntimeException(getMessage("DNDBM905"));
    } catch (NullPointerException ne) {
      logger.error(getMessage("DNDBM903", dspName));
      throw new DndRuntimeException(getMessage("DNDBM903", dspName));
    }

    try {
      adTypes = Arrays.asList(parameters.get(1));
    } catch (ArrayIndexOutOfBoundsException e) {
      adTypes = Arrays.asList("banner", "native", "video");
    }
  }

  protected void downloadS3(String s3Bucket, File file, String s3Prefix) throws Exception {
    try {
      if (s3Accessor.existNotRetry(S3ObjectName.of(s3Bucket, s3Prefix, file.getName()))) {
        s3Accessor.download(s3Bucket, s3Prefix, file);
      } else {
        logger.warn(getMessage("DNDBM108", file.getName()));
      }
    } catch (Exception e) {
      logger.warn(getMessage("DNDBM902", file.getName()), e);
      throw new DndRuntimeException(getMessage("DNDBM902", file.getName()));
    }
  }

  protected void bkupS3(String s3Bucket, File file, String srcPrefix, String dstPrefix) throws Exception {
    try {
      if (s3Accessor.existNotRetry(S3ObjectName.of(s3Bucket, srcPrefix, file.getName()))) {
        s3Accessor.bkup(s3Bucket, srcPrefix, file.getName(),
          String.join("", "bk/", dstPrefix),
          String.join("_", file.getName(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHMMSS")))
        );
      } else {
        logger.warn(getMessage("DNDBM011", file.getName()));
      }
    } catch (Exception e) {
      logger.warn(getMessage("DNDBM906", file.getName()), e);
      throw new DndRuntimeException(getMessage("DNDBM906", file.getName()));
    }
  }

  protected class StopWatcher extends Thread {

    protected boolean stopRequested;

    protected DndJdgBatch batch;

    protected boolean isStopRequested() {
      return stopRequested;
    }

    protected void shutdown() {
      stopRequested = true;
    }

    @Override
    public void run() {
      stopRequested = false;

      while (!stopRequested) {
        if (checkStop()) {
          stopRequested = true;
          batch.requestStop();
          logger.info(getMessage("DNDBM005"));
          break;
        }

        try {
          Thread.sleep(5000);
        } catch (InterruptedException e) {
          break;
        }
      }
    }
  }

  @SuppressWarnings("serial")
  protected class StopRequestException extends RuntimeException {

  }
}
