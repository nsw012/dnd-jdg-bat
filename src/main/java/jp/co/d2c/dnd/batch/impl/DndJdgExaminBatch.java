package jp.co.d2c.dnd.batch.impl;

import jp.co.d2c.dnd.service.ExaminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static jp.co.d2c.dnd.util.MessageHelper.getMessage;


/**
 * 自動審査バッチ
 */
@Component("DNDJDGEXM")
public class DndJdgExaminBatch extends DndJdgBatch {

  @Autowired
  private ExaminService examinService;

  @Override
  protected void doInTemplate() {

    parameter();
    List<String> outputs = adTypes.stream().map(e -> {
      if (e.equals("banner")) return "banner";
      if (e.equals("native")) return "native";
      if (e.equals("video")) return "video";
      return null;
    }).collect(Collectors.toList());

    logger.info(getMessage("DNDBM300", dspName));

    outputs.stream()
      .filter(i -> Optional.ofNullable(i).isPresent())
      .forEach(f -> {
        logger.info(getMessage("DNDBM301", f));
        try {

          // Banner
          Optional.ofNullable(f)
            .filter(ff -> ff.equals("banner"))
            .ifPresent(fff -> examinService(() -> examinService.examinBanner()));

          // Native
          Optional.ofNullable(f)
            .filter(ff -> ff.equals("native"))
            .ifPresent(fff -> examinService(() -> examinService.examinNative()));

          // Video
          Optional.ofNullable(f)
            .filter(ff -> ff.equals("video"))
            .ifPresent(fff -> examinService(() -> examinService.examinVideo()));

          logger.info(getMessage("DNDBM302", f));

        } catch (Exception e) {
          logger.info(getMessage("DNDBM803", f));
          e.printStackTrace();
        }
      });
    logger.info(getMessage("DNDBM307", dspName));
  }

  private void examinService(Runnable examin) {
    examinService.init(dspId);
    examin.run();
    logger.info(getMessage("DNDBM308", examinService.getCounters().getTotal()));
  }

}