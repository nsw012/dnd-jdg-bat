/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.batch;

/**
 * 非常駐バッチを実装するための抽象クラス
 */
public abstract class NonResidentBatchTemplate extends BatchBase {

  public NonResidentBatchTemplate(long waitPrepare) {
    super(waitPrepare);
  }

  public NonResidentBatchTemplate() {
    super();
  }

  @Override
  public void doInExecute() throws Exception {
    doInTemplate();
  }

  protected abstract void doInTemplate();
}
