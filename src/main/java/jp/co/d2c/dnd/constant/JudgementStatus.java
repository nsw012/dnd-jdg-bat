package jp.co.d2c.dnd.constant;

public enum JudgementStatus implements CodeEnum {
  /**
   * 対象外
   */
  EXCLUDED,
  /**
   * 未審査(配信不可)
   */
  UNEXAMINED,
  /**
   * 事前承認(配信可)
   */
  PRE_APPROVED,
  /**
   * 承認(配信可)
   */
  APPROVED,
  /**
   * 否認(配信不可)
   */
  REJECTED;

  @Override
  public String keyPrefix() {
    return "viewCode";
  }
}