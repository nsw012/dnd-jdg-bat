/*
 * Copyright (c) D2C Inc 2015.
 */
package jp.co.d2c.dnd.constant;

/**
 * コード定数を定義するインターフェース
 */
public interface CodeConstant<T> {

  int getValue();

  T valueOf(int status);
}
