package jp.co.d2c.dnd.constant;

/**
 * 列挙型インターフェイス。
 */
public interface CodeEnum {

  int ordinal();

  default String keyPrefix() {
    return "code";
  }

  default String keySuffix() {
    return String.valueOf(ordinal());
  }

  default String codeKey() {
    return keyPrefix() + "." + getClass().getSimpleName() + "." + keySuffix();
  }
}