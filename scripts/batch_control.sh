#!/bin/sh

if [ $(id -u) -eq 0 ];
then
  echo "root ユーザでは実行できません"
  exit 1
fi

export LANG=ja_JP.UTF-8
#export PATH=/home/ec2-user/java/jre1.8.0_51/bin:$PATH

### define valiables
BATCH_HOME='../'
LIB_DIR=${BATCH_HOME}/lib
ENV_LIST='./envlist'
JAR='dnd-jdg-bat-2.0.0-assembly.jar'
MAIN_CLASS='jp.co.d2c.dnd.batch.BatchRunner'
BATCH_STATUS_READY=0
BATCH_STATUS_RUNNING=1
BATCH_STATUS_STOP_WAIT=2

. ./java_options

### define functions
print_usage() {
  echo "Usage: `basename $0` [--start batch_code params...] [--process-check] [--process-detail]" 1>&2
  exit 1
}

print_log_message() {
  echo "`date "+%Y-%m-%d %H:%M:%S"` $1" | tee -a ${BATCH_HOME}/scripts/batch_control.out
}

start() {
  local BATCH_CODE=$1
  if [ "${BATCH_CODE}" = "" ];then
    print_log_message "BATCH_CODEを指定してください"
  else
    shift
    print_log_message "${BATCH_CODE} を起動します $@"
    JAVA_OPTIONS=`eval echo '$'${BATCH_CODE}'_OPTIONS'`
    nohup java -Duser.timezone=JST ${JAVA_OPTIONS} -cp ${LIB_DIR}/${JAR} ${MAIN_CLASS} ${BATCH_CODE} `hostname` $@ >> ./batch_control.out 2>&1 &
    print_log_message "${BATCH_CODE} を $! で起動しました"
  fi
}

process_check() {
  local CNT_FILE=/tmp/$$
  local CNT=0
  echo ${CNT} > ${CNT_FILE}
  ps -ef | grep "jp\.co\.d2c\.dnd\.batch\.BatchRunner" | while read line; do
    echo "$line" | sed -e "s/^.*BatchRunner[ \t]\+\([A-Z0-9]\+\)[ \t].*$/\1/g"
    CNT=`expr ${CNT} + 1`
    echo ${CNT} > ${CNT_FILE}
  done
  CNT=`cat ${CNT_FILE}`
  rm ${CNT_FILE}
  if [ ${CNT} -eq 0 ]
  then
    echo "実行中のプロセスはありません"
  else
    echo "${CNT} プロセスが実行中です"
  fi
}

process_detail() {
  local CNT_FILE=/tmp/$$
  local CNT=0
  echo ${CNT} > ${CNT_FILE}
  ps -ef | grep "jp\.co\.d2c\.dnd\.batch\.BatchRunner" | while read line; do
    BCD=`echo "$line" | sed -e "s/^.*BatchRunner[ \t]\+\([A-Z0-9]\+\)[ \t].*$/\1/g"`
    BID=`echo "$line" |  awk '{ print $2 }'`
    MEM=`top -b -n 1 | grep "^ *${BID} " | awk '{print $5,$6,$9"%"}'`
    echo "${BCD}: ${BID} (${MEM})"
    CNT=`expr ${CNT} + 1`
    echo ${CNT} > ${CNT_FILE}
  done
  CNT=`cat ${CNT_FILE}`
  rm ${CNT_FILE}
  if [ ${CNT} -eq 0 ]
  then
    echo "実行中のプロセスはありません"
  else
    echo "${CNT} プロセスが実行中です"
  fi
}

### main process

. ${ENV_LIST}

while :
do
  case $1 in
    --start)
      shift
      start $@
      break
      ;;
    --process-check)
      process_check
      break;
      ;;
    --process-detail)
      process_detail
      break;
      ;;
    *)
      print_usage
      exit 1
      ;;
  esac
done
