#!/bin/bash

### define valiables
BATCH_HOME="/home/ec2-user/plabat"
LOG_DIR=${BATCH_HOME}/logs
ROOM_ID="65605947"
ACCESS_TOKEN="7a7c60dc9e3c710b58e3d72bdfb874e6"
TO="[To:1213851][To:1398895][To:1313467]"
TITLE="[title]PLAエラー情報[/title]"
MSG=""

### define functions
function usage {
  echo "Usage: `basename $0`" 1>&2
  exit 1
}

function post2chat {
  ERR_CNT=`grep "PLABM119" ${LOG_DIR}/BPLA000*.log | wc -l`
  if [ $ERR_CNT -eq 0 ]; then
    MSG="[info]${TITLE}商品の入稿に失敗したフィードはありません[/info]"
  else
    MSG="${TO}[info]${TITLE}商品の入稿に失敗したフィードが${ERR_CNT}件ありました[/info]"
  fi
  curl -X POST -H "X-ChatWorkToken:${ACCESS_TOKEN}" -d "body=${MSG} " "https://api.chatwork.com/v2/rooms/${ROOM_ID}/messages"
}

### main
if [ $# -eq 0 ]; then
  post2chat
else
  usage
fi

