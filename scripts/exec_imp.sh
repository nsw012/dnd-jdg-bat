#!/bin/bash

function process_check {
  while true
  do 
   sleep 3;
   if ./batch_control.sh --process-check | grep "実行中のプロセスはありません" >/dev/null 2>&1; then
     break;
   fi
  done
}

#test1
./batch_control.sh --start DNDJDGIMP test1
process_check
./info_chat.sh DNDJDGIMP

#kande
./batch_control.sh --start DNDJDGIMP kanade
process_check
./info_chat.sh DNDJDGIMP

#fout
./batch_control.sh --start DNDJDGIMP fout
process_check
./info_chat.sh DNDJDGIMP

