#!/bin/bash

### define valiables
BATCH_HOME="../"
LOG_DIR=${BATCH_HOME}/scripts/logs
ROOM_ID="80759542"
ACCESS_TOKEN="af387a6364bbfec44a362d7c7801dbdc"

### define functions
function usage {
  echo "Usage: `basename $0` [batch_code]" 1>&2
  exit 1
}

function postchat {
  if [[ ${BATCH_CODE} = "DNDJDGIMP" ]]; then
    postimp
  elif [[ ${BATCH_CODE} = "DNDJDGEXP" ]]; then
    postexp
  elif [[ ${BATCH_CODE} = "DNDJDGEXM" ]]; then
    postexm
  fi
}

function postimp {

  START=`grep -h -n -e "DNDBM100" ${LOG_DIR}/DNDJDGIMP.log |tail -1|cut -d ":" -f 1`
  DNDBM100=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "DNDBM100" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`
  DNDBM109=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "DNDBM109" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`
  DNDBM107=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "DNDBM107" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`
  DNDBM104=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "DNDBM104" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`
  DNDBM801=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "DNDBM801" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`
  DNDBM106=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "DNDBM106" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`
  PROCTIME=`tail -n +${START} ${LOG_DIR}/DNDJDGIMP.log|grep -h -e "経過時間" |sed 's/^\(.*\)DNDJDGIMP.*DndJdgImportBatch/\1/'`

  RESULT=${DNDBM100}${IFS}${DNDBM109}${IFS}${DNDBM107}${IFS}${DNDBM104}${IFS}${DNDBM801}${IFS}${DNDBM106}${IFS}${PROCTIME}
  RESULT=`echo "${RESULT}"|grep -v -e '^\s*#' -e '^\s*$'`

  TITLE="[title]DNDJDG 審査バッチ実行結果 ${BATCH_CODE}[/title]"
  MSG="[info]${TITLE}${RESULT}[/info]"

  curl -X POST -H "X-ChatWorkToken:${ACCESS_TOKEN}" -d "body=${MSG} " "https://api.chatwork.com/v2/rooms/${ROOM_ID}/messages"
}

function postexp {

  START=`grep -h -n -e "DNDBM200" ${LOG_DIR}/DNDJDGEXP.log |tail -1|cut -d ":" -f 1`
  DNDBM200=`tail -n +${START} ${LOG_DIR}/DNDJDGEXP.log|grep -h -e "DNDBM200" |sed 's/^\(.*\)DNDJDGEXP.*DndJdgExportBatch/\1/'`
  DNDBM109=`tail -n +${START} ${LOG_DIR}/DNDJDGEXP.log|grep -h -e "DNDBM109" |sed 's/^\(.*\)DNDJDGEXP.*DndJdgExportBatch/\1/'`
  DNDBM206=`tail -n +${START} ${LOG_DIR}/DNDJDGEXP.log|grep -h -e "DNDBM206" |sed 's/^\(.*\)DNDJDGEXP.*DndJdgExportBatch/\1/'`
  DNDBM207=`tail -n +${START} ${LOG_DIR}/DNDJDGEXP.log|grep -h -e "DNDBM207" |sed 's/^\(.*\)DNDJDGEXP.*DndJdgExportBatch/\1/'`
  DNDBM802=`tail -n +${START} ${LOG_DIR}/DNDJDGEXP.log|grep -h -e "DNDBM802" |sed 's/^\(.*\)DNDJDGEXP.*DndJdgExportBatch/\1/'`
  PROCTIME=`tail -n +${START} ${LOG_DIR}/DNDJDGEXP.log|grep -h -e "経過時間" |sed 's/^\(.*\)DNDJDGEXP.*DndJdgExportBatch/\1/'`

  RESULT=${DNDBM200}${IFS}${DNDBM109}${IFS}${DNDBM206}${IFS}${DNDBM802}${IFS}${PROCTIME}
  RESULT=`echo "${RESULT}"|grep -v -e '^\s*#' -e '^\s*$'`

  TITLE="[title]DNDJDG 審査バッチ実行結果 ${BATCH_CODE}[/title]"
  MSG="[info]${TITLE}${RESULT}[/info]"

  curl -X POST -H "X-ChatWorkToken:${ACCESS_TOKEN}" -d "body=${MSG} " "https://api.chatwork.com/v2/rooms/${ROOM_ID}/messages"
}

function postexm {

  START=`grep -h -n -e "DNDBM300" ${LOG_DIR}/DNDJDGEXM.log |tail -1|cut -d ":" -f 1`
  DNDBM300=`tail -n +${START} ${LOG_DIR}/DNDJDGEXM.log|grep -h -e "DNDBM300" |sed 's/^\(.*\)DNDJDGEXM.*DndJdgExaminBatch/\1/'`
  DNDBM308=`tail -n +${START} ${LOG_DIR}/DNDJDGEXM.log|grep -h -e "DNDBM308" |sed 's/^\(.*\)DNDJDGEXM.*DndJdgExaminBatch/\1/'`
  DNDBM307=`tail -n +${START} ${LOG_DIR}/DNDJDGEXM.log|grep -h -e "DNDBM307" |sed 's/^\(.*\)DNDJDGEXM.*DndJdgExaminBatch/\1/'`
  DNDBM803=`tail -n +${START} ${LOG_DIR}/DNDJDGEXM.log|grep -h -e "DNDBM803" |sed 's/^\(.*\)DNDJDGEXM.*DndJdgExaminBatch/\1/'`
  PROCTIME=`tail -n +${START} ${LOG_DIR}/DNDJDGEXM.log|grep -h -e "経過時間" |sed 's/^\(.*\)DNDJDGEXM.*DndJdgExaminBatch/\1/'`

  RESULT=${DNDBM300}${IFS}${DNDBM308}${IFS}${DNDBM307}${IFS}${DNDBM803}${IFS}${PROCTIME}
  RESULT=`echo "${RESULT}"|grep -v -e '^\s*#' -e '^\s*$'`

  TITLE="[title]DNDJDG 審査バッチ実行結果 ${BATCH_CODE}[/title]"
  MSG="[info]${TITLE}${RESULT}[/info]"

  curl -X POST -H "X-ChatWorkToken:${ACCESS_TOKEN}" -d "body=${MSG} " "https://api.chatwork.com/v2/rooms/${ROOM_ID}/messages"
}

### main
if [ $# -eq 1 ]; then
  BATCH_CODE=$1
  postchat
else
  usage
fi
