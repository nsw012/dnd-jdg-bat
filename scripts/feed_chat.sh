#!/bin/bash

### define valiables
export LANG=ja_JP.UTF-8
ENV_LIST='./envlist'
FEED_LIST='./feedlist'

BATCH_HOME="../"
LOG_DIR=${BATCH_HOME}/logs
ROOM_ID="65605947"
ACCESS_TOKEN="7a7c60dc9e3c710b58e3d72bdfb874e6"
TO="[To:1213851][To:1398895][To:1313467]"
TITLE="[title]フィード更新情報[/title]"
MSG=""
### 実行月以降でフィード更新あるかを確認する
CONDITION_DATE=$(date '+%Y-%m-01 00:00:00')

### define functions
function usage {
  echo "Usage: `basename $0`" 1>&2
  exit 1
}

function execute_sql {
  local COMMAND="select feed_id from product where feed_id not in (${ACTIVE_FEED_LIST}) and last_update_date >= '${CONDITION_DATE}' group by feed_id;"
  local HOST=`echo ${DATASOURCE_URL} | sed -n 's/^.*\/\/\(.*\)\:.*$/\1/p'`
  local DATABASE=`echo ${DATASOURCE_URL} | sed -n 's/^.*\/\(.*\)?.*$/\1/p'`
  mysql --defaults-file=./my.cnf -u ${DATASOURCE_USERNAME} -h ${HOST} -D ${DATABASE} -e "${COMMAND};"
}

function post2chat {
  RESULT=` execute_sql`
  if [ -z "${RESULT}" ]; then
    MSG="[info]${TITLE}自動連携していないフィードはありません[/info]"
  else
    RESULT=` echo "${RESULT}" | sed -e "s/feed_id//g"`
    MSG="${TO}[info]${TITLE}自動連携していないフィードが追加されています${RESULT}[/info]"
  fi
  curl -X POST -H "X-ChatWorkToken:${ACCESS_TOKEN}" -d "body=${MSG} " "https://api.chatwork.com/v2/rooms/${ROOM_ID}/messages"
}

### main
. ${ENV_LIST}
. ${FEED_LIST}
if [ $# -eq 0 ]; then
  post2chat
else
  usage
fi
