#!/bin/bash

function process_check {
  while true
  do 
   sleep 3;
   if ./batch_control.sh --process-check | grep "実行中のプロセスはありません" >/dev/null 2>&1; then
     break;
   fi
  done
}

#test1
./batch_control.sh --start DNDJDGEXP test1
process_check
./info_chat.sh DNDJDGEXP

#kande
./batch_control.sh --start DNDJDGEXP kanade
process_check
./info_chat.sh DNDJDGEXP

#fout
./batch_control.sh --start DNDJDGEXP fout
process_check
./info_chat.sh DNDJDGEXP

